\chapter{Performance Analysis and Results}
\label{chap.analysis}
This chapter presents an analysis of the performance achieved by the developed parallelization strategy for the
PajeNG's tool-set. The Section~\ref{sec:platform} presents the experimental
methodology and the two platforms that were used to conduct the
experiments. Then, in the Section~\ref{sec:scalability}, we
start the results presentation with an overview of the performance obtained
with speedup and efficiency graphs. In the following Sections~\ref{sec:Locality} and~\ref{sec:readNUMA}, we
investigate the reason for the performance drop from a certain level
of parallelism. The latter led us to correlate the application behavior with architectural details suggesting changes to
our strategy. Finally, the Section~\ref{sec:disk}
considers disk measurements, comparing the application performance when the input data are on HDD versus SSD.

\section{Experimental Platform and Methodology}\label{sec:platform}
The settings of the two experimental platforms that were used for the
analysis are summarized in Table~\ref{table.platform}. The first is a
Dell PowerEdge R910 machine identified as {\bf turing} with 32
physical cores arranged in four Intel(R) Xeon(R) CPU X7550 of 2.00GHz,
each with eight cores. The nodes have non-uniform memory access
(NUMA), and each core is in a NUMA node. Regarding the numbering
of the physical cores for NUMA nodes in the case of turing, the cores
are identified using an offset of 4 by 4. The main memory of
the system is 128GBytes. The solid disc is a Corsair Force 3, with 64
GBytes, while the hard drive is a Hitachi HUC151414CSS60 and Western
Digital WD10TPVT-00U configured in RAID. The second platform is an SGI
C1104G-RP5 machine identified as {\bf bali} with 16 physical cores
arranged in two processors Intel(R) Xeon(R) E5-2650 2GHz, each with
eight cores. This machine has two NUMA nodes. The numbering
of the physical cores for NUMA nodes in the case of bali, the cores are identified using an offset of 8 by 8. The main memory of the
system is 64GBytes. The solid drive is a Samsung SSD 840 500GBytes
while the hard drives are two Seagate ST91000640NS 1 TBytes each.

\def\tabularxcolumn#1{m{#1}}
\begin{table}[!htb]\label{table.platform}
\caption{Experimental platform settings.}
\centering
  \begin{tabularx}{\textwidth}{XXX} \toprule
    & {\bf turing}        & {\bf bali} \\\toprule
    Brand                   & Dell PowerEdge R910 & SGI C1104G-RP5 \\
    Processor               & Intel X7550 2GHz    & Intel E5-2650 2Ghz \\
    Turbo Boost Technology  & v1.0                & v2.0 \\
    Max Turbo Frequency     & 2.4Ghz              & 2.8Ghz \\
    Cache L3 Size           & 18MB                & 20MB \\
    Bus Speed               & 6.4 GT/s QPI        & 8 GT/s QPI  \\ \hline                  
    NUMA Nodes              & 4                   & 2 \\
    Cores/Node              & 8                   & 8 \\
    Total Cores             & 32                  & 16 \\
    NUMA Nodes Config       & 0: 0, 4, 8, 12, 16, 20, 24, 28 \newline 1: 1, 5, 9, 13, 17, 21, 25, 29 \newline 2: 2, 6, 10, 14, 18, 22, 26, 30 \newline 3: 3, 7, 11, 15, 19, 23, 27, 31                   & 0: 0, 1, 2, 3, 4, 5, 6, 7 \newline 1: 8, 9, 10, 11, 12, 13, 14, 15 \\
    DMA Disk Controller     & Node 0              & Node 0 \\
    \hline
    Memory                  & DDR3                & DDR3 \\
    Memory Size             & 128 GBytes          & 64 GBytes \\
    Memory Speed            & 1066MHz             & 1600MHz \\
    \hline
    Hard drive              & RAID Dell PERC H700 \newline 4TBytes                & 2 x Seagate ST91000640NS \newline 2 x 1TBytes \\
    Solid drive             & Corsair Forse 3 \newline 64GBytes & Samsung SSD 840 \newline 500GBytes
    \\\bottomrule
  \end{tabularx}
\end{table}

Each experimental set was performed at least 30 times in order to
decrease the possibility that measurement errors disturb the
interpretation. The preliminary analysis of each battery of tests have
identified that most of the observations follows a Gaussian
distribution indicating normality. The observations that do not fit
perfectly into a normal distribution are those where the execution
overload and the performance bottleneck appear, which this chapter
intends to present. The experimental methodology follows some factors:

\begin{description}
\item[Locality] The threads locality, also known as
  affinity, has two levels of measurement: with free-running threads
  (\emph{free}), where the system scheduler can migrate these threads to
  optimize the run-time and memory access, or pinned threads (\emph{pin}), where we
  define a specific mapping in the beginning of the execution that
  does not change. In this second case, a linear mapping was done
  according to the numbering of the cores provided by the operational
  system. It is important to note that the numbering provided in
  turing platform differs from the bali.
\item[Quantity] The amount of threads was evaluated using five
  levels comprising: a sequential implementation with only one execution
  flow (1), which was used as reference for the speedup calculation, and then a parallel version with 2,
  4, 8 and 16 threads.
\item[Disc Type] This factor compares the solid drives and hard drives of the machines. The main goal is to compare the
  reading performance depending on the technology and
  the architectures of each platform.
\end{description}

\section{Scalability Overview}\label{sec:scalability}
This section presents an overview about the speedup and efficiency
achieved by the parallelization strategy of the PajeNG. The used
measurements for such overview assumes that the trace data is in
memory. This is not the most realistic approach since generally the
data will be on the disk, however with that we can explicit the limits
of our strategy. The overview is divided in two subsections: the
Subsection~\ref{sub:strong} with strong
scalability~\ref{sec:AmdahlLaw} analysis and Subsection~\ref{sub:weak}
with weak scalability~\ref{sec:GustafsonLaw} results.

\subsection{Strong Scalability Overview}\label{sub:strong}

This subsection considers strong scalability measurements, that is the
run-time versus number of execution flows with a fixed input about 10
GBytes. Besides that, for these experiments we do not have disabled
the Linux operation system's cache and the automatic thread
scheduling. This means that the files are automatically buffered in
memory and the threads are free to migrate among cores according to
the operating system rules. So these
results consider run-times for \emph{free} threads and with the
input data stored in memory independently of the machine's disk type. Figure~\ref{fig:speedup} presents the speedup
achieved by our strategy on each machine, bali and turing (see platform description in Section~\ref{sec:platform}). The \textit{X} axis presents the number of threads while
the \textit{Y} axis shows the respective speedup. These results come from the minimum value of each
experiment. The minimum was chosen because frequently worst results
are caused by some intrusion and so the average can not be a good
measurement. The grey line
illustrates what should be the ideal speedup and the other ones the
results for each machine. In the Figure~\ref{fig:speedup} we can see
that both machines still speeding up until four threads and then there
is a performance drop. The machine bali continues to gain
performance until sixteen threads but far away from
the ideal speedup. On the other hand, the machine turing drastically slows down with eight and sixteen threads, getting closer
to the sequential results. Such observations will be further discussed
in the Section~\ref{sec:Locality} and~\ref{sec:readNUMA},
with more detailed data that can better explain these results.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=1\textwidth]{imgs/graph001-strong-speedup-jonas-diss}}
  \caption{Strong scaling speedup graph for bali and
    turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:speedup}
\end{figure}

The Figure~\ref{fig:efficiency} presents the efficiency results of our
strategy. Like the speedup (see Figure~\ref{fig:speedup}), the plotted
results consider the minimum value from thirty executions for each
configuration. The \textit{X} axis presents the number of threads,
while the \textit{Y} axis the respective efficiency. The grey line
demonstrates what should be the ideal efficiency and the other ones
the results for each machine. Obviously we know that the efficiency
graph~\ref{fig:efficiency} will show the same patter observed for the
speedup~\ref{fig:speedup}, achieving a relatively good efficiency
until four threads and then there is an efficiency drop. However this
graph~\ref{fig:efficiency} exposes an interesting behavior for the
machines: until four threads the machine turing has achieved
a better efficiency than bali, but when the parallelism increases, turing loses too much performance. Thereby the state changes
and bali ends achieving much better efficiency.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=1\textwidth]{imgs/graph002-strong-efficiency-jonas-diss}}
  \caption{Strong scaling efficiency graph for bali and turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:efficiency}
\end{figure}

\subsection{Weak Scalability Overview}\label{sub:weak}
This subsection considers weak scalability experiments, that means
increase the problem size proportionally to the number of execution
flows. Like the strong scalability~\ref{sub:strong} measurements, for
this overview we do not disable operation system's disk cache and the
automatic thread's scheduling. The Figure~\ref{fig:sizeup} shows the
capability of increase the input size(\textit{sizeup}) achieved by our
strategy on each machine, bali and turing. The \textit{X} axis presents the number of threads while the
\textit{Y} axis shows the respective \textit{sizeup} capability. These
results also came from the minimum value of each experiment, all with
thirty executions for each configuration. The input size of each
experimental setting is displayed over the ideal \textit{sizeup} grey
line, starting from 150MBytes for the sequential until 2400MBytes for 16
threads. In
the Figure~\ref{fig:sizeup} we can note that the weak scalability
experiments for bali have followed a similar pattern of the
strong speedup results as seen in Subsection~\ref{sub:strong},
\review{? ¿ State the proper noun.? ¿ \\ Don't understand \\ It is to change "Section" by "Subsection" or by Strong Scalability Subsection??} scaling up the input size
until near eight times and then losing performance. On the other hand,
the weak measurements for turing have a different behavior,
\review{Every time you refer to a section, uses the proper noun before
  it.} allowing a \textit{sizeup}
about near eight times. Other interesting observation is that for all
experiments, both strong and weak scalability, the machine
turing has achieved better results until at least four
threads, but when the parallelism increases, the turing's performance drastically decreases.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=1\textwidth]{imgs/graph003-weak-sizeUp-jonas-diss}}
  \caption{Weak scaling \textit{sizeup} graph for bali and
    turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:sizeup}
\end{figure}

The Figure~\ref{fig:weakefficiency} presents the efficiency graph for
weak scalability results from both machines bali and
turing.  The \textit{X} axis
presents the number of threads while the \textit{Y} axis shows the
respective efficiency. These results are also considering the minimum time for each
configuration. The input size of each experimental setting is
displayed on the top of the Figure~\ref{fig:weakefficiency}, over the grey
line that illustrates the ideal efficiency.

\review{You should explain the axis in first place, then state the results. This is valid for all figures.}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=1\textwidth]{imgs/graph004-weak-efficiency-sizeUp-jonas-diss}}
  \caption{Weak scaling efficiency graph for bali and
    turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:weakefficiency}
\end{figure}

\section{Threads Locality Interference }\label{sec:Locality}
This section deepens the performance analysis and intends to
explain why we have obtained the results presented in the
overview Section~\ref{sec:scalability}. For this we have formulated
the hypothesis that since the tasks are very memory bounded, its
migration could be a point of performance loss. To prove such
hypothesis we have performed experiments by fixing (\emph{Pin}) each
thread on a core, thus preventing that the operating system
to migrate (\emph{Free}) them. The results for this section still
consider that the operational system's data cache is enabled, so
the input trace files are kept in memory. Furthermore, for these
experiments, the pinning process of the threads was made sequentially, thus ignoring the architectural structure of the machines (see Section~\ref{sec:platform} and Table~\ref{table.platform} in line NUMA Nodes Config). In other words,
each thread was fixed in the first available core id, for example: thread
1 in core 1, thread 2 in core 2 and so on.

The Figure~\ref{fig:PinvsFreeSpeedup} presents the speedup obtained by
the experiments with each thread fixed (\emph{Pin}) on a core, in
contrast to the results for \emph{Free} threads. The
Figure~\ref{fig:PinvsFreeSpeedup} is divided in two plots: in the left
are the results for the machine bali and in the right for the
turing. The \textit{X} axis shows the number of threads and
the \textit{Y} axis the respective speedup. The grey lines for both
plots are illustrating the ideal speedup, and the other lines, the
speedup achieved for each setting: \emph{Free} and
pinned (\emph{Pin}) threads. The \emph{Pin} threads
version have achieved better speedup results than the \emph{Free} ones. For
example: with the \emph{Pin} version for the machine turing
we were able to reach a nearly linear speedup until eight threads, while with the \emph{Free} just four. In turn, the machine
bali does not present much differences between the versions,
just with a little higher speedup for the \emph{Pin} version with
sixteen threads. Thereby, these results reinforce our hypothesis that
the threads migration are causing performance loss. However the machine
turing still presenting huge performance loss after eight
threads for both settings, \emph{Pin} and \emph{Free}. Thus, from
now on we will focus on turing since it is the bigger machine
and have more interesting results. The next
Figure~\ref{fig:readDuration} shows more detailed measurements
comparing the data read's time duration between each turing's NUMA
node.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=1\textwidth]{imgs/graph005-PinvsFreeSpeedup-jonas-diss}}
  \caption{Strong scaling speedup Pin vs Free Threads graph for
    bali and turing machines. Thirty executions for
    each configuration, all without drop the operational system data
    cache.}
  \label{fig:PinvsFreeSpeedup}
\end{figure}

The results obtained from turing with \emph{Pin} threads
experiments led us to formulate a new hypothesis based on the
turing's NUMA nodes organization (see Section~\ref{sec:platform} and Table~\ref{table.platform} in line NUMA Nodes Config): since the NUMA node 0 is the only one directly
connected to the disk IO interface, the others 1, 2 and 3, maybe need
more time to access some data that were not found in the cached memory and
have to be loaded from the disk, thus causing performance
loss. Furthermore, we have pinned the threads sequentially and the
cores's id/NUMA nodes are organized in a different way. For example: core 0 is on NUMA 0, core 1 on NUMA
1 and core 2 on NUMA 2, core 3 on NUMA 3, core 4 comes back to NUMA 0
and so on until reach 16 cores, 8 physical and 8 logical for each of 4
NUMA nodes. Based on this hypothesis we have performed experiments
that collect the data's read duration time for each NUMA node
intending to show the differences between them. The
Figure~\ref{fig:readDuration} presents the results for such
experiment. The \textit{X} axis shows the NUMA node id and the
\textit{Y} axis its respective average data's read duration time. The
vertical lines represent the standard error of each data-set. The
average read time duration of the NUMA node 0 is really lower than the others. The
NUMA node 1 also presents better results than both 2 and 3, and is the
only one that comes closer to the node 0. However this probably
happens due to a physical proximity of the two nodes. The next
Section~\ref{sec:readNUMA} presents some more experiments that will
consider the turing's NUMA node configuration, thus trying to
achieve better results.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=1\textwidth]{imgs/graph006-ReadDuration-allNUMA-jonas-diss}}
  \caption{Comparison between NUMA node's average read duration Time
    from bali and turing machines. Thirty executions
    for each configuration, all without drop the operational system
    data cache.}
  \label{fig:readDuration}
\end{figure}

% \begin{figure}[!htb]
%   \centering
%   \begin{tabular}{hh}
% \centering
% % these plots were generated with bali1-turing2-6seq-speedup.r
% \includegraphics[width=.8\linewidth]{imgs/graph005-PinvsFreeSpeedup-jonas-diss}&
% \includegraphics[width=.8\linewidth]{imgs/graph007-PinvsFreeEfficiency-jonas-diss}
% \\
% (a) & (b) 
% \end{tabular}
% \caption{Aceleração e eficiência medidas nas plataformas bali e
% turing.}
% \label{fig.speedup-efficiency}
% \end{figure}


\section{Differences Between NUMA nodes Read
  Throughput}\label{sec:readNUMA}
This section explores a different thread's pinning approach for experiments performed in the machine turing. We have focused on the machine turing since it has produced very strange results and so, we want to explain what is happening and causing the performance drop from 8 to 16 threads. Therewith, this section take into account the turing's architecture, more specifically the NUMA nodes/cores distribution, as described in Section~\ref{sec:platform}, Table~\ref{table.platform}. The latter consists in the thread's pinning to use only the turing's NUMA nodes 0 and 1. Therewith we intend to achieve better performance due to the experiments exposed in the above section by the Figure~/ref{fig:readDuration}, in which the nodes 0 and 1 have presented lower read times.

The Figure~/ref{fig:speedup2NUMAvsAllNUMA} presents the speedup achieved by the experiments pinning threads in all NUMA nodes (0, 1, 2, 3) in contrast to only nodes 0 and 1.  The \textit{X} axis shows the number of threads and
the \textit{Y} axis the respective speedup. The grey line illustrates the ideal speedup, and the other lines, the results obtained for each pinning approach. The Figure~/ref{fig:speedup2NUMAvsAllNUMA} confirms that the performance increases when we have the threads fixed (pinned) only in the NUMA nodes 0 and 1. On the other hand, the performance dropping from 8 to 16 threads, albeit with less intensity, keeps being observed.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph014-Turing2NUMAvsTuringAllNUMA-jonas-diss}}
  \caption{Speedup achieved by pinning threads in all NUMA nodes (0, 1, 2, 3) versus only the nodes 0 and 1.}
  \label{fig:speedup2NUMAvsAllNUMA}
\end{figure}

\section{Disk Analysis}\label{sec:disk}
finishing early tomorrow

\chapter{Introduction}
\label{chap.introduction}
The continuous increasing demand of computing power tends to the
development of efficient parallel applications that better use the
available resources. Modern high performance systems comprise
thousands to millions of processing units connected by complex
hierarchical networks. The development of parallel applications for
such systems is a complicated task, affected by at least two factors:
the first is the high scalability provided by them which is hard to
achieve; the second is the lack of a deterministic execution. Since
processing nodes are independent, the understanding of such
applications can be very painful. Some libraries like
OpenMP~\cite{dagum1998openmp} and MPI~\cite{gropp1999mpi2} are used to
facilitate the implementation of these applications. However, even
with supporting libraries, the successful development of a high
performance parallel application depends on an accurate mapping of
application processes on top of available resources.

The identification of unused resources and potential processing
bottlenecks requires good performance analysis tools that are able to
observe many entities over long periods of time. This observation is
often initiated by collecting significant performance measurements
through event tracing. The collected data is usually
from the application level. Tracing tools commonly register
local and global states of the program, the amount of data transferred
by messages and hardware counters for specific functions. This data enables the
detection of several complex patterns, such as late communications,
costly synchronization or train
effects~\cite{schnorr2012detection}. Unfortunately,  tracing
often produces large trace files, easily reaching the order of gigabytes of
raw data. Therefore the processing of such data to a human readable way
should be efficient to allow a quick and useful performance analysis.

Over the years, several tracing tools have been developed for
performance analysis of parallel applications. The major challenges
of these tools are the processing and interpretation of the large amount
of data produced by parallel applications. The data is frequently used to
replay/simulate the behavior of the program execution. This 
results in visual representations such as space/time views,
communication graphs among others~\cite{schnorr2009}. Most of the
existing tools such as Vampir~\cite{muller2007developing},
Scalasca~\cite{scalasca_2010}, TAU~\cite{shende2006tau} have focus
on the processing of trace formats with a fixed and well-defined semantic. The
corresponding file format are usually proposed to handle applications developed
using popular libraries like OpenMP, MPI, and CUDA. However, not all parallel
applications use such libraries and so, sometimes, these tools cannot be
used. Parallel runtimes provided by StarPU~\cite{augonnet2011starpu}
and XKaapi~\cite{gautier2013xkaapi} are examples where usual HPC tracing
tools are uncapable to obtain correct performance data. Fortunetaly,
there are other tools that present a more dynamic
approach. They have an open trace file format without being
associated with any specific semantic. This way they can handle with a wide range of
parallel applications including parallel runtimes such as those
mentionned before. Some of these tools are:
Paraver~\cite{pillet1995paraver}, Paje~\cite{paje2000} and
PajeNG~\cite{pajeng}. The fact of being more generic comes with a
cost. These tools very frequently present
low performance for the processing of large traces. In some occasions,
they handle the increasing data size by applying aggregation
techniques which can remove important performance information from the
original trace data.

This work focuses in the optimization of the PajeNG's toolset, more
specifically the Paje simulator responsible for replaying traces and
re-creating the original behavior of the observed parallel application. This
toolset works with the Pajé trace format~\cite{pajetracefile} which
has an open semantic. PajeNG relies on its Paje simulator and built on
top of that tools for performance analysis. Examples of such tools are
the space/time view provided by \texttt{pajeng}  and
\texttt{pj\_dump}. The latter is capable to export
performance data in a CSV-like textual format well tailored to 
conduct R analysis~\cite{ihaka1996r}. The current implementation of
the Paje simulator is sequential and therefore not scalable when dealing
with large trace files. This dissertation presents a
parallelization strategy for PajeNG, intending to improve its
performance and scalability. This document is organized as follows.

% Here we present how the original
% PajeNG , the developed strategy, and also a performance analysis for
% the parallel version. The analysis comprises different aspects and
% presents very promising results, scaling up the PajeNG about eight to
% ten times depending on the machine.

\begin{description}

\item[Chapter~\ref{chap.BasicConcepts}: Basic Concepts.]
The main purpose of this chapter is to present basic concepts that
are important for a good understading of this work. It explains two
ways of how to measure application performance, and it also provides
an overview about the Non-Uniform Memory Access (NUMA) architecture.
\item[Chapter~\ref{chap.relatedwork}: Related Work.]
This chapter makes an overview about the current scenario of
performance analysis tools based on trace replay. The most common
approaches used to build such tools with high performance are
presented. The chapter also presents the most common parallelization
patterns/techniques.
\item[Chapter~\ref{chap.proposal}: Parallel Trace Replay in PajeNG.]
This chapter presents the implemented parallelization technique for
the Paje simulation library. It starts by the description of how the
original implementation of PajeNG works, followed by the description
of optimizations made by our work.
\item[Chapter~\ref{chap.analysis}: Performance Analysis and Results.]
This chapter presents a performance analysis of our solution. We
detail and explain the impressive results, scaling up to eight times the
original PajeNG with the developed
strategy. The chapter initiates describing the experimental platform
and the performance methodology. Then it continues to an overview
about the achieved speedup and efficiency, and terminates by a more
detailed analysis including disk measurements and comparisons
between different NUMA nodes.
\item[Chapter~\ref{chap.conclusion}: Conclusion.]
This chapter lists the main contributions of this
work. It also includes some ideas for future work.
\end{description}
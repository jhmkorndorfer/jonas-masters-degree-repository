\chapter{Performance Analysis and Results}
\label{chap.analysis}
This chapter presents an analysis of the performance achieved by the developed parallelization strategy for the
PajeNG's tool-set. Section~\ref{sec:platform} presents the experimental
methodology and the two platforms that were used to conduct the
experiments. Section~\ref{sec:scalability} proposes an overview of the performance obtained
with speedup and efficiency graphs. In Sections~\ref{sec:Locality} and~\ref{sec:readNUMA}, we
investigate the reason for the performance drop from a certain level
of parallelism. The latter led us to correlate the application behavior with architectural details suggesting changes to
our strategy. Finally, the Section~\ref{sec:disk}
considers disk measurements, comparing the application performance when the input data are on HDD versus SSD.

\section{Experimental Platform and Methodology}\label{sec:platform}
The settings of the two experimental platforms that were used for the
analysis are summarized in Table~\ref{table.platform}. The first is a
Dell PowerEdge R910 machine identified as {\bf turing} with 32
physical cores arranged in four Intel(R) Xeon(R) CPU X7550 of 2.00GHz,
each with eight cores. The nodes have non-uniform memory access
(NUMA), and each processor is in a NUMA node. 
Cores are identified across NUMA nodes using an offset of 4 by 4. The main memory of
the system is 128GBytes. The solid disc is a 64 GBytes Corsair Force 3,
while the hard drive is a Hitachi HUC151414CSS60 and Western
Digital WD10TPVT-00U configured as RAID 1. The second platform is an SGI
C1104G-RP5 machine identified as {\bf bali} with 16 physical cores
arranged in two processors Intel(R) Xeon(R) E5-2650 2GHz, each with
eight cores. This machine has two NUMA nodes.
Cores are identified across NUMA nodes using an offset of 8 by 8. The main memory of the
system is 64GBytes. The solid drive is a Samsung SSD 840 500GBytes
while the hard drives are two Seagate ST91000640NS 1 TBytes each.

\def\tabularxcolumn#1{m{#1}}
\begin{table}[!htb]\label{table.platform}
\caption{Experimental platform settings.}
\centering
  \begin{tabularx}{\textwidth}{XXX} \toprule
    & {\bf turing}        & {\bf bali} \\\toprule
    Brand                   & Dell PowerEdge R910 & SGI C1104G-RP5 \\
    Processor               & Intel X7550 2GHz    & Intel E5-2650 2GHz \\
    Turbo Boost Technology  & v1.0                & v2.0 \\
    Max Turbo Frequency     & 2.4GHz              & 2.8GHz \\
    Cache L3 Size           & 18MB                & 20MB \\
    Bus Speed               & 6.4 GT/s QPI        & 8 GT/s QPI  \\ \midrule
    NUMA Nodes              & 4                   & 2 \\
    Cores/Node              & 8                   & 8 \\
    Total Cores             & 32                  & 16 \\
    NUMA Nodes Config       & 0: 0, 4, 8, 12, 16, 20, 24, 28 \newline 1: 1, 5, 9, 13, 17, 21, 25, 29 \newline 2: 2, 6, 10, 14, 18, 22, 26, 30 \newline 3: 3, 7, 11, 15, 19, 23, 27, 31                   & 0: 0, 1, 2, 3, 4, 5, 6, 7 \newline 1: 8, 9, 10, 11, 12, 13, 14, 15 \\
    DMA Disk Controller     & Node 0              & Node 0 \\
    \midrule
    Memory                  & DDR3                & DDR3 \\
    Memory Size             & 128 GBytes          & 64 GBytes \\
    Memory Speed            & 1066MHz             & 1600MHz \\
    \midrule
    Hard drive              & RAID Dell PERC H700 \newline 4TBytes                & 2 x Seagate ST91000640NS \newline 2 x 1TBytes \\
    Solid drive             & Corsair Forse 3 \newline 64GBytes & Samsung SSD 840 \newline 500GBytes
    \\\bottomrule
  \end{tabularx}
\end{table}

Each experimental combination was executed at least 30 times in order to
decrease the possibility that measurement errors disturb the
interpretation. The preliminary analysis of each test battery have
identified that most of the observations follows a Gaussian
distribution indicating normality. The observations that do not fit
perfectly into a normal distribution are those where the execution
overload and the performance bottleneck appear, which this chapter
intends to present. The input data for the
Sections~\ref{sec:scalability},~\ref{sec:Locality},~\ref{sec:readNUMA}
was a trace file of about 10GBytes divided in 64 pieces of 150MBytes
each. For the disk measurements Section~\ref{sec:disk}, the input data
was a trace file of about 2GBytes split in 64 pieces of 32MBytes
each. These differences of input size do not influence the analysis
results, this was made just to generate the results more quickly,
since the disk experiments need longer periods of time to execute. The
experimental methodology is tailored to consider the following factors:

\begin{description}
\item[Locality] The threads locality, also known as
  affinity, has two levels of measurement: with free-running threads
  (\emph{free}), where the system scheduler can migrate these threads to
  optimize the run-time and memory access, or pinned threads (\emph{pin}), where we
  define a specific mapping in the beginning of the execution that
  does not change. In this second case, a linear mapping was done
  according to the numbering of the cores provided by the operational
  system.
\item[Quantity] The amount of threads was evaluated using five
  levels comprising: a sequential implementation with only one execution
  flow, which was used as reference for the speedup calculation, and then a parallel version with two,
  four, eight and sixteen threads.
\item[Disc Type] This factor compares the solid drives and hard drives of the machines. The main goal is to compare the
  reading performance depending on the disk technology of each platform.
\end{description}

\section{Scalability Overview}\label{sec:scalability}
This section presents an overview about the speedup and efficiency
achieved by the parallelization strategy of the PajeNG. The used
measurements for such overview assumes that the trace data is in
memory. This is not the most realistic approach since generally the
data will be on the disk, however with that we can explicit the limits
of our strategy. The overview is divided in two subsections: the
Subsection~\ref{sub:strong} with strong
scalability~\ref{sec:AmdahlLaw} analysis and Subsection~\ref{sub:weak}
with weak scalability~\ref{sec:GustafsonLaw} results.

\subsection{Strong Scalability Overview}\label{sub:strong}

This subsection considers strong scalability measurements, that is the
run-time versus number of execution flows with a fixed input about 10
GBytes. Besides that, for these experiments we do not have disabled
the Linux operation system's cache and the automatic thread
scheduling. This means that the files are automatically buffered in
memory and the threads are free to migrate among cores according to
the operating system rules. So these
results consider run-times for \emph{free} threads and with the
input data stored in memory independently of the machine's disk type. Figure~\ref{fig:speedup} presents the speedup
achieved by our strategy on each machine, bali and turing (see platform description in Section~\ref{sec:platform}). The \textit{X} axis presents the number of threads while
the \textit{Y} axis shows the respective speedup. These results come from the minimum value of each
experiment. The minimum was chosen because frequently worst results
are caused by some intrusion and so the average can not be a good
measurement. The grey line
illustrates what should be the ideal speedup and the other ones the
results for each machine. In the Figure~\ref{fig:speedup} we can see
that both machines are still speeding up until four threads and then there
is a performance drop. The machine bali continues to gain
performance until sixteen threads but far away from
the ideal speedup. On the other hand, the machine turing drastically slows down with eight and sixteen threads, getting closer
to the sequential results. Such observations will be further discussed
in the Section~\ref{sec:Locality} and~\ref{sec:readNUMA},
with more detailed data that can better explain these results.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph001-strong-speedup-jonas-diss}}
  \caption{Strong scaling speedup graph for bali and
    turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:speedup}
\end{figure}

The Figure~\ref{fig:efficiency} presents the efficiency results of our
strategy. Like the speedup (see Figure~\ref{fig:speedup}), the plotted
results consider the minimum value from thirty executions for each
configuration. The \textit{X} axis presents the number of threads,
while the \textit{Y} axis the respective efficiency. The grey line
demonstrates what should be the ideal efficiency and the other ones
the results for each machine. Efficiency (see
Figure~\ref{fig:efficiency})
shows the same pattern observed for the
speedup depicted in Figure~\ref{fig:speedup}, but in a different
manner. It achieves a relatively good efficiency
until four threads and then there is an efficiency drop. However this
graph~\ref{fig:efficiency} exposes an interesting behavior for the
machines: until four threads the machine turing has achieved
a better efficiency than bali, but when the parallelism is increased,
turing loses much more performance. Thereby the state changes
and bali ends achieving much better efficiency.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph002-strong-efficiency-jonas-diss}}
  \caption{Strong scaling efficiency graph for bali and turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:efficiency}
\end{figure}

\subsection{Weak Scalability Overview}\label{sub:weak}
This subsection considers weak scalability experiments, that means
increase the problem size proportionally to the number of parallel execution
flows. Like the strong scalability~\ref{sub:strong} measurements, for
this overview we do not disable operation system's disk cache and the
automatic thread's scheduling. The Figure~\ref{fig:sizeup} shows the
capability of increase the input size(\textit{sizeup}) achieved by our
strategy on each machine, bali and turing. The \textit{X} axis presents the number of threads while the
\textit{Y} axis shows the respective \textit{sizeup} capability. These
results also came from the minimum value of each experiment, all with
thirty executions for each configuration. The input size of each
experimental setting is displayed over the ideal \textit{sizeup} grey
line, starting from 150MBytes for the sequential until 2400MBytes for 16
threads. In
the Figure~\ref{fig:sizeup} we can note that the weak scalability
experiments for bali have followed a similar pattern of the
strong speedup results as seen in Subsection~\ref{sub:strong}, scaling up the input size
until eight times and then losing performance. On the other hand,
the weak measurements for turing have a different behavior, allowing a \textit{sizeup}
about eight times. Other interesting observation is that for all
experiments, both strong and weak scalability, the machine
turing has achieved better results until four
threads, but when the parallelism increases, the turing's performance drastically decreases.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph003-weak-sizeUp-jonas-diss}}
  \caption{Weak scaling \textit{sizeup} graph for bali and
    turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:sizeup}
\end{figure}

The Figure~\ref{fig:weakefficiency} presents the efficiency graph for
weak scalability results from both machines bali and
turing.  The \textit{X} axis
presents the number of threads while the \textit{Y} axis shows the
respective efficiency. These results are also considering the minimum time for each
configuration. The input size of each experimental setting is
displayed on the top of the Figure~\ref{fig:weakefficiency}, over the grey
line that illustrates the ideal efficiency.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph004-weak-efficiency-sizeUp-jonas-diss}}
  \caption{Weak scaling efficiency graph for bali and
    turing machines. Thirty executions for each
    configuration, all without drop the operational system data
    cache.}
  \label{fig:weakefficiency}
\end{figure}

\section{Threads Locality Interference }\label{sec:Locality}
This section deepens the performance analysis and intends to
explain why we have obtained the results presented in the
overview Section~\ref{sec:scalability}. For this we have formulated
the hypothesis that since the tasks are memory bounded, its
migration could be a point of performance loss. To prove such
hypothesis we have performed experiments by fixing (\emph{Pin}) each
thread on a core, thus preventing the operating system
to migrate (\emph{Free}) them. The results for this section still
consider that the operational system's data cache is enabled, so
the input trace files are kept in memory. Furthermore, for these
experiments, the pinning process of the threads was made sequentially, thus ignoring the architectural structure of the machines (see Section~\ref{sec:platform} and Table~\ref{table.platform} in line NUMA Nodes Config). In other words,
each thread was fixed in the first available core identifier, for example: thread
1 in core 1, thread 2 in core 2 and so on.

The Figure~\ref{fig:PinvsFreeSpeedup} presents the speedup obtained by
the experiments with each thread fixed (\emph{Pin}) on a core, in
contrast to the results for \emph{Free} threads. The
Figure~\ref{fig:PinvsFreeSpeedup} is divided in two plots: in the left
are the results for the machine bali and in the right for the
turing. The \textit{X} axis shows the number of threads and
the \textit{Y} axis the respective speedup. The grey lines for both
plots are illustrating the ideal speedup, and the other lines, the
speedup achieved for each setting: \emph{Free} and
pinned (\emph{Pin}) threads. The \emph{Pin} threads
version have achieved better speedup results than the \emph{Free} ones. For
example: with the \emph{Pin} version for the machine turing
we were able to reach a nearly linear speedup until eight threads, while with the \emph{Free} just four. In turn, the machine
bali does not present much differences between the versions,
just with a little higher speedup for the \emph{Pin} version with
sixteen threads. Thereby, these results reinforce our hypothesis that
the threads migration are causing performance loss. However the machine
turing still presenting huge performance loss after eight
threads for both settings, \emph{Pin} and \emph{Free}. Thus, from
now on we will focus on turing since it is the bigger machine
and have more interesting results. The next
Figure~\ref{fig:readDuration} shows more detailed measurements
comparing the data read's time duration between each turing's NUMA
node.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=1\textwidth]{imgs/graph005-PinvsFreeSpeedup-jonas-diss}}
  \caption{Strong scaling speedup Pin vs Free Threads graph for
    bali and turing machines. Thirty executions for
    each configuration, all without drop the operational system data
    cache.}
  \label{fig:PinvsFreeSpeedup}
\end{figure}

The results obtained from turing with \emph{Pin} threads
experiments led us to formulate a new hypothesis based on the
turing's NUMA nodes organization (see Section~\ref{sec:platform} and
Table~\ref{table.platform} in the line NUMA Nodes Config). 
The NUMA node 0 is the only one directly
connected to the disk IO interface. The remaining NUMA nodes (1, 2,
and3) may require more time to access data, thus causing performance
loss. Furthermore, we have pinned the threads sequentially and the
cores's identifiers/NUMA nodes are organized in a different way.
For example: core 0 is on NUMA 0, core 1 on NUMA
1 and core 2 on NUMA 2, core 3 on NUMA 3, core 4 comes back to NUMA 0
and so on until the counting reaches 16 cores, 8 physical and 8
logical
for each of 4
NUMA nodes. Based on this hypothesis we have performed experiments
that collect the data's read duration time for each NUMA node
intending to show the differences between them. The
Figure~\ref{fig:readDuration} presents the results for such
experiment. The \textit{X} axis shows the NUMA node identifier and the
\textit{Y} axis its respective average data's read duration time. The
whiskers represent the standard error of each data-set, calculated as
three times the standard deviation divided by the square root of the
number of experiments.
The average read time duration of the NUMA node 0 is lower than the others. The
NUMA node 1 also presents better results than both 2 and 3, and is the
only one that comes closer to the node 0. However this probably
happens due to a physical proximity of the two nodes. The next
Section~\ref{sec:readNUMA} presents some more experiments that will
consider the turing's NUMA node configuration, thus trying to
achieve better performance.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph006-ReadDuration-allNUMA-jonas-diss}}
  \caption{Comparison between turing's NUMA node's average read time
    duration in seconds. Thirty executions
    for each configuration, all without drop the operational system's
    data cache.}
  \label{fig:readDuration}
\end{figure}


\section{Differences Between NUMA nodes Read
  Throughput}\label{sec:readNUMA}
This section explores a different thread's pinning approach for
experiments performed in the turing machine. We have focused on the
machine turing since it has produced very strange results and so, we
want to explain what is happening and causing the performance drop
from 8 to 16 threads. This section takes into account the
turing's architecture, more specifically the NUMA nodes/cores
distribution, as described in Section~\ref{sec:platform},
Table~\ref{table.platform}. The latter consists in the thread's
pinning to use only the turing's NUMA nodes 0 and 1. We
intend to achieve better performance due to the experiments exposed in
the above section by the Figure~\ref{fig:readDuration}, in which the
nodes 0 and 1 have presented lower read times.

The Figure~\ref{fig:speedup2NUMAvsAllNUMA} presents the speedup
achieved (on $Y$) as a function of the number of threads (on $X$) by
the experiments when pinning threads in all NUMA nodes (0, 1, 2, 3) in
contrast to only NUMA nodes 0 and 1.   The
grey line illustrates the ideal speedup, and the other lines, the
results obtained for each pinning approach. The
Figure~\ref{fig:speedup2NUMAvsAllNUMA} confirms that the performance
increases when we have the threads fixed (pinned) only in the NUMA
nodes 0 and 1. On the other hand, the performance dropping from 8 to
16 threads, albeit with less intensity, keeps being observed.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph014-Turing2NUMAvsTuringAllNUMA-jonas-diss}}
  \caption{Speedup achieved by pinning threads in all NUMA nodes (0,
    1, 2, 3) versus only the nodes 0 and 1.}
  \label{fig:speedup2NUMAvsAllNUMA}
\end{figure}

The experiments performed on turing using only the NUMA nodes 0 and 1 have achieved a little better speedup than the ones that have used all nodes. However the observed pattern of performance drop from 8 to 16 threads occurs in both experiments. Therewith we have decided to compare the average read time duration of the experiments configured to use all NUMA nodes with the ones that use only the nodes 0 and 1. This observation should present lower read times for the experiments with only the nodes 0 and 1. If true, the latter will prove our hypothesis that the nodes 0 and 1 have better read capabilities than the others, and thus they end up being faster. This experiment is depicted in the Figure~\ref{fig:readAvsRead2}. The \textit{X} axis presents the experiment set name like \texttt{turing\_allNUMA} (nodes 0, 1, 2, 3) and \texttt{turing\_NUMA0-1} (nodes 0, 1). The \textit{Y} axis shows the average read time for each set of experiments. Unfortunately, the Figure~\ref{fig:readAvsRead2} has showed an unexpected behavior, presenting higher read times for the experiments performed with only the NUMA nodes 0 and 1. The fact is that this results end up refuting our hypothesis for the performance drop in the machine turing.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph015-AVDURATIONT52NUMAvsT6AllNUMA-jonas-diss}}
  \caption{Comparison between the average read time duration in seconds of the experiments with turing's NUMA nodes 0 and 1 versus all nodes.}
  \label{fig:readAvsRead2}
\end{figure}

The exact reason for the performance drop in the machine turing still
under study. We have performed some more experiments but none were
able to provide the problem. This problem was not observed in the
machine bali. Therewith, we consider some differences between the
machines intending to formulate a new hypothesis for future
studies. All experiments performed so far have considered that the
traces are kept in main memory. Thus, there is a good hypothesis for
the bali's better performance, since its memory/bus speeds (1600MHz -
8GT/s QPI) are very faster than the turing ones (1066MHz - 6.4GT/s
QPI). Furthermore bali is a little bit newer machine with more cache
(20MB versus 18MB on turing) and the turbo boost technology v2.0 that
can increase the processor's speed to 2.8GHz, versus the v1.0 present
on turing that just achieves 2.4GHz. Therefore, we conclude that since
the developed strategy is very memory bounded, the turing's lower
memory/bus/processor speeds may be the bottleneck for our approach.

\section{Disk Analysis}\label{sec:disk}

This section presents experiments performed by using the
command \begin{center}\texttt{sysctl vm.drop\_caches=3}\end{center}
before each experimental execution. This command drops the operational
system data cache, thus forcing the data reads directly from the disks
where they are kept. The input data for the disk measurements were
smaller than the others experiments to decrease the experimental
time. Measurements have used traces of about 2GBytes divided in 64 pieces of 32MBytes each. The section discusses the relation between the disk type and the application performance, and also compares the read time duration versus number of threads. The latter considers HDD and SSD measurements for both machines (\texttt{bali} and \texttt{turing}) and settings (\texttt{Pin} and \texttt{Free}).

The Figure~\ref{fig:speedupDisk} displays the speedup achieved by our
strategy assuming that the data were not cached for both machines, for
turing (a) and bali (b). In these two plots, (a) and (b), the
\textit{X} axis presents the number of threads, and the \textit{Y}
axis, the speedup obtained. The grey lines represent the ideal speedup and the other lines the results for \texttt{Free} or \texttt{Pin} threads versions. The left side of the graphs shows the results considering the input data stored on HDD, while the right side considers the results for SSD. These experiments provide information for several conclusions about our strategy. First of all, we can see that the HDD results for both machines were bad, achieving a speedup of only about 4. These results were expected since rarely HDDs can handle with multiple accesses at same time, thus when we increase the number of threads, the concurrence grows degrading the performance. On the other hand, the speedup achieved on the SSDs were good: reaching 8 times in both \texttt{Free} and \texttt{Pin} settings for the machine turing and about 10 times for bali. The next graphs make a closer look into the behavior of each disk type by considering their read time duration for different number of threads, thus allowing a precise identification of the performance features for each one.

\begin{figure}[!htb]
\centering
\begin{tabular}{c}%x}{\textwidth}{XXX}
\setlength\tabcolsep{0pt} 
\includegraphics[width=.7\linewidth]{imgs/graph019-turingHDDvsSSD-PINvsFREE-jonas-diss} 
\\(a) \\
\includegraphics[width=.7\linewidth]{imgs/graph020-baliHDDvsSSD-PINvsFREE-jonas-diss}
\\(b)
\end{tabular}
\caption{(a) Speedup achieved on turing with the input trace files
  kept on disk - SSD vs HDD - Pin vs Free; (b) Same but for bali.}
\label{fig:speedupDisk}
\end{figure}

% \begin{figure}[!htb]
%   \centerline{\includegraphics[width=.8\textwidth]{imgs/graph019-turingHDDvsSSD-PINvsFREE-jonas-diss}}
%   \caption{Speedup achieved on turing with the input trace files kept on disk - SSD vs HDD - Pin vs Free.}
%   \label{fig:diskTuringSpeedup}
% \end{figure}

% \begin{figure}[!htb]
%   \centerline{\includegraphics[width=.8\textwidth]{imgs/graph020-baliHDDvsSSD-PINvsFREE-jonas-diss}}
%   \caption{Speedup achieved on bali with the input trace files kept on disk - SSD vs HDD - Pin vs Free.}
%   \label{fig:diskBaliSpeedup}
% \end{figure}


The Figures~\ref{fig:diffHDDTuring} and~\ref{fig:diffHDDBali} depicts
HDD measurements about the thread's read time duration from the
machines turing and bali respectively. The results were classified
considering three settings: \texttt{Pin}, \texttt{Free} and
\texttt{Seq} (sequential). They show the relation between
the average read time with an increasing number of threads. In both
figures the \textit{X} axis presents the number of threads, and the
\textit{Y} axis, the respective average read time. These figures also
display rectangles which are zoom zones to expose values that are much
smaller. For example,
in the Figure~\ref{fig:diffHDDTuring} the rectangle comprises read
times from 1 to 2 threads, and in the Figure~\ref{fig:diffHDDBali}
from 1 to 4, which are the smaller values of each one. The presented
results explain the speedup's behavior for HDDs. As we can
see in the Figure~\ref{fig:speedupDisk} (HDD, (a) and (b)), the
speedup keeps growing until 4 threads, which is exactly the maximum
number of threads for which the reads duration is still stable. Besides that,
the speedup drops from 4 to 16 threads due to the huge growth of the
read time duration from there on. The latter proves the problems from
HDDs which can not work with multiple accesses, as observed for both
machines.


\begin{figure}[!htb]
\centering
\includegraphics[width=.7\linewidth]{imgs/graph027-AvHDDReads-Turing-PinFree-jonas-diss}
\caption{The average read time duration in seconds for HDD as a function of the number of
  threads for turing.}
\label{fig:diffHDDTuring}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=.7\linewidth]{imgs/graph028-AvHDDReads-Bali-PinFree-jonas-diss}
\caption{The average read time duration in seconds for HDD as a function of the number of
  threads for bali.}
\label{fig:diffHDDBali}
\end{figure}

% \begin{figure}[!htb]
% \centering
% \begin{tabular}{c}%x}{\textwidth}{XXX}
% \setlength\tabcolsep{0pt} % default value: 6pt
% %this plot is generated with turing-variability.r
% \includegraphics[width=.8\linewidth]{imgs/graph027-AvHDDReads-Turing-PinFree-jonas-diss}
% \\(a) \\
% %these plots were generated with turing3-read-duration.r
% \includegraphics[width=.8\linewidth]{imgs/graph028-AvHDDReads-Bali-PinFree-jonas-diss}
% \\(b)
% \end{tabular}
% \caption{(a) turing's diff HDD read time duration ranging the number of threads; (b) bali's diff HDD read time duration ranging the number of threads.}
% \label{fig.diffHDD}
% \end{figure}

%\begin{figure}[!htb]
%\centering
%\includegraphics[width=.7\linewidth]{imgs/graph023-AvHDDReads-Turing-PinFree-jonas-diss}
%\caption{turing's diff HDD read time duration ranging the number of threads.}
%\label{fig:diffHDDTuring2}
%\end{figure}

%\begin{figure}[!htb]
%\centering
%\includegraphics[width=.7\linewidth]{imgs/graph025-AvHDDReads-Bali-PinFree-jonas-diss}
%\caption{bali's diff HDD read time duration ranging the number of threads.}
%\label{fig:diffHDDBali2}
%\end{figure}


% \begin{figure}[!htb]
% \centering
% \begin{tabular}{c}%x}{\textwidth}{XXX}
% \setlength\tabcolsep{0pt} % default value: 6pt
% %this plot is generated with turing-variability.r
% \includegraphics[width=.8\linewidth]{imgs/graph023-AvHDDReads-Turing-PinFree-jonas-diss}
% \\(a) \\
% %these plots were generated with turing3-read-duration.r
% \includegraphics[width=.8\linewidth]{imgs/graph025-AvHDDReads-Bali-PinFree-jonas-diss}
% \\(b)
% \end{tabular}
% \caption{(a) turing's diff HDD read time duration ranging the number of threads; (b) bali's diff HDD read time duration ranging the number of threads.}
% \label{fig.diffHDD2}
% \end{figure}

The Figures~\ref{fig:diffSSDTuring} and~\ref{fig:diffSSDBali} are presenting the SSD measurements about the thread's read time duration. These results were also classified considering the settings \texttt{Pin}, \texttt{Free} and \texttt{Seq}. These show the behavior of our strategy when running on SSDs with an increasing number of threads. Like the HDD graphs, the figures expose in the \textit{X} axis, the number of threads, and in the \textit{Y} axis, the respective average read time. These figures do not display the zoom rectangles since the measurements for all settings were more stable and closer each others. The results can also explain the performance obtained by our strategy when the traces were stored in SSDs. The speedup graph, Figure~\ref{fig:speedupDisk} (SSD, (a) and (b)), shows that the performance gain keeps growing until at least 8 threads in both machines, and then they slow down. This behavior is easily understood when we take a look to the Figures~\ref{fig:diffSSDTuring} and~\ref{fig:diffSSDBali}, which show that the max number of threads with small read times are in fact 8. Furthermore, from 8 to 16 threads the read times increase faster, and thus, the speedup drops. The latter proves the better performance of SSDs when handle with multiple accesses, however it is important to note that the sequential read times are very similar for both disks.



\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph022-AvSSDReads-Turing-PinFree-jonas-diss}}
  \caption{The average read time duration in seconds for SDD as a function of the number of
  threads for turing.}
  \label{fig:diffSSDTuring}
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph024-AvSSDReads-Bali-PinFree-jonas-diss}}
  \caption{The average read time duration in seconds for SDD as a function of the number of
  threads for bali.}
  \label{fig:diffSSDBali}
\end{figure}


\section{Analysis Summary}\label{sec:analysisSummary}

The performance analysis has focused to provide experimental results
showing the gains obtained with the implemented parallel strategy for
the PajeNG simulator. The analysis has considered two platforms and
several aspects like the number of execution flows (threads), locality
and disk type. The Table~\ref{table:analysisSummary} summarizes our
results by presenting the maximum speedup obtained for each platform
and for each configuration of storage system/threads locality. These
results show us that the machines have presented different
behavior. The machine bali, in most of the cases, continues gaining
speedup until 16 threads, however each time with less efficiency. On
the other hand, the machine turing reaches a certain limit with about
8 threads with good efficiency, and then, with more threads, it loses
performance. This chapter has also presented an investigation over the
application's behavior running on the turing host. We intented to
understand the loss of performance detected in this machine. This
investigation is still being carried out since the performed
experiments have not showed sufficiently conclusive results yet.

% \subsection{Classification}\label{table:classification}
\def\tabularxcolumn#1{m{#1}}
\begin{table}[!htb]\label{table:analysisSummary}
  \caption{Experimental results summary}
  \centering
  \begin{tabularx}{\textwidth}{lXXXXl} \toprule
    {\bf Machine} & {\bf Trace Data In} & {\bf Locality} & {\bf Max Speedup} & {\bf Efficiency}\\\toprule
    bali & Memory\newline Memory\newline SSD\newline SSD\newline HDD\newline HDD & Free\newline Pin\newline Free\newline Pin\newline Free\newline Pin & 7.04 (8 t)\newline 9.21 (16 t)\newline 9.88 (16 t)\newline 11.87 (16 t)\newline 3.20 (8 t)\newline 3.20 (8 t) & 44\%\newline 58\%\newline 61\%\newline 74\%\newline 40\%\newline 40\%\\
    \midrule
    turing & Memory\newline Memory\newline SSD\newline SSD\newline HDD\newline HDD & Free\newline Pin\newline Free\newline Pin\newline Free\newline Pin & 3.63 (4 t)\newline 7.16 (8 t)\newline 7.65 (8 t)\newline 7.62 (8 t)\newline 2.76 (4 t)\newline 2.37 (4 t) & 90\%\newline 89\%\newline 95\%\newline 95\%\newline 69\%\newline 59\%\\
    \bottomrule
  \end{tabularx}
\end{table}
% \begin{figure}[!htb]
% \centering
% \begin{tabular}{c}%x}{\textwidth}{XXX}
% \setlength\tabcolsep{0pt} % default value: 6pt
% %this plot is generated with turing-variability.r
% \includegraphics[width=.8\linewidth]{imgs/graph022-AvSSDReads-Turing-PinFree-jonas-diss}
% \\(a) \\
% %these plots were generated with turing3-read-duration.r
% \includegraphics[width=.8\linewidth]{imgs/graph024-AvSSDReads-Bali-PinFree-jonas-diss}
% \\(b)
% \end{tabular}
% \caption{(a) turing's diff SSD read time duration ranging the number of threads; (b) bali's diff SSD read time duration ranging the number of threads.}
% \label{fig.diffSSD}
% \end{figure}

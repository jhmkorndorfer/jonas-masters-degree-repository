# README #
### Jonas's Master's Degree Repository###

* Papers
* Experiments
* Scripts
* Graphs
* Dissertation

### By downloading this repository, almost everything can be found and executed from the LabBook.org file (Org-mode). ###
* [Good tutorial to install and use Org-mode with Emacs](http://mescal.imag.fr/membres/arnaud.legrand/misc/init.php)
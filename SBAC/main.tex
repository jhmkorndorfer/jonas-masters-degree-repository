
\documentclass[conference]{IEEEtran}




\ifCLASSINFOpdf
   \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../pdf/}{../jpeg/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
   \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi


\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}



\title{Parallel Trace Replay for PajeNG on Shared-Memory Architectures}




\author{\IEEEauthorblockN{Jonas H. M. Korndorfer}

\IEEEauthorblockA{Instituto de Inform\'atica \\ Universidade Federal do Rio Grande do Sul\\
Email: jhmkorndorfer@inf.ufrgs.br}
\and
\IEEEauthorblockN{Lucas Mello Schnorr}
\IEEEauthorblockA{Instituto de Inform\'atica \\ Universidade Federal do Rio Grande do Sul\\
Email: schnorr@inf.ufrgs.br}
}




\maketitle


\begin{abstract}

Achieve high performance in massive parallel applications is a great challenge. Such programs are too complex to be analysed and optimized without proper techniques and help tools. One of the most helpful techniques is the trace-based behavioral observation of the program execution. This approach frequently have to deal with huge amounts of raw data, which creates another challenge: how to process such information in an efficient way. Here we present an optimized version of a trace-based performance analysis tool, the PajeNG. The original PajeNG's version generates nice representations by replaying the trace data. Even so its processing model is sequential and scalable to a certain limit, spending an unacceptable time to process too large traces. Therewith the paper presents a parallelization strategy developed to improve the performance and scalability of the PajeNG. We explain what were the changes in the initial tool, from the new work-flow until the way that the traces are organized. The major approached aspects are the scalability and speedup reached by the strategy. For this several experiments have been performed producing quite optimistic results. Finally, we comment on the potential for future works created due to our strategy.
\end{abstract}




\IEEEpeerreviewmaketitle


\section{Introduction}
The current increasing application demands of computing power are tending for the development of parallel outings~\cite{sadashiv2011cluster} that better use the multi-core architectures existing today. Such development is complex task. Two factors have great impact for this complexity. The first is the high scalability of current supercomputers. The second is the lack of deterministic execution: the processing nodes are independent, so two executions with the same entry can demonstrate completely different behavior. Some libraries like OpenMP~\cite{dagum1998openmp} and MPI~\cite{gropp1999mpi2} are used to help the development of such parallel applications. However even with such libraries, the success of the development of a high performance parallel application depends on a correct mapping of the available resources.

The behavioral understanding of the application execution is one of the most powerful methods for the identification of potential bottlenecks and misused resources. Frequently this is done by tracing an application collecting performance counters from the execution. This method results in thousands gigabytes of raw performance data that must be processed. Thus the processing of such data must achieve high performance.

In this scenario, over the years, several tools were developed to help the performance analysis of parallel application. One of the most common approaches is the generation of visualizations based on the traces. Such implementations are frequently focused in analysis to specific libraries like MPI or OpenMP. In this work we present a remodeling of a behavioral observation tool based on trace replay the PajeNG. This tool has a great feature that is its dynamism being able to handle with several libraries due to its own trace file format with no semantics associated. On the other hand the PajeNG works sequentially and has some problems to process large trace data. So here we propose a parallel implementation of the PajeNG and its performance analysis.

This text is structured as follows. Section~\ref{sec:relatedwork} presents parallelization or distribution efforts on other performance analysis tools based on trace replay. Section~\ref{sec:pajeseq} details how the original PajeNG works internally. Section~\ref{sec:pajepar} explains how works our parallelization strategy and what were the modifications in the original PajeNG. The section~\ref{sec:results} perform some performance analysis on our strategy showing several graphs with the results. Finally we conclude in the section~\ref{sec:conclusion} with some considerations about this work and how it can help future works.


\section{Related Work}\label{sec:relatedwork}



\section{Original PajeNG Implementation}\label{sec:pajeseq}
PajeNG~\cite{pajeng} is a tool-set for trace replay built in C++. It works with its own trace file format~\cite{pajetracefile} and provides four components with different proposals: \textit{pj\_validate}, \textit{pj\_dump}, \textit{pajeng}, \textit{libpaje}. The \textit{pj\_validate} is used to analyse the integrity of the trace data. The \textit{pj\_dump} can export the results of the processing in a CSV file format. The \textit{pajeng} is a visualization tool that generates visual representations of the replayed traces. Finally the \textit{libpaje}, which is the  great focus of this work, it is an open source library that does the traces processing in all other tools contained in the PajeNG. Furthermore it is object oriented and can be easily modified or extended. This section is structured as follows............


\subsection{Description of the Paj\'e Trace Format}\label{traceformat}
The Paj\'e trace file format is a self-defined, textual, and generic format to describe the behavior of the execution of parallel and/or distributed systems. The traces in the Paje format are subdivided in five conceptual objects that are:
\begin{itemize}
     \item Container: can be any monitored entity like a network link, a process, or a thread. It is the most generic object and the unique that can hold others, including other containers.
     \item State: represents anything that has a beginning and ending time-stamp. For example: a given container starts blocked and then, after some time, change to free, this period of time characterises a state.
     \item Event: is anything remarkable enough to be uniquely identified and having only one time-stamp always associated to a container.
     \item Variable: represents the evolution of the value of a variable along time.
     \item Link: represents the relation between two containers heaving a having a beginning and ending timestamps.
\end{itemize}

\subsection{Work-flow Features \& Problems}\label{sec:pajeseqW}
The \textit{libpaje}, responsible for the trace replay, works with one trace file each time and processes this in a sequential way. This is done by three main classes: \textit{PajeFileReader}, \textit{PajeEventDecoder} and \textit{PajeSimulator}. The figure~\ref{fig:pajengseq} illustrates the work-flow followed by the \textit{libpaje}, proceeding from the left to the right. The \textit{PajeFileReader}, left of the figure~\ref{fig:pajengseq}, initiates the processing, reading chunks of the trace file and then sending them to the second class. In the next step, the \textit{PajeEventDecoder} class receives the chunks and splits them into lines which are transformed in generic event objects. After that, such events are sent to the final step. Lastly, the \textit{PajeSimulator} class receives each event object, places them in their respective containers and attaches its respective features.

\begin{figure}[!htb]
\includegraphics[width=0.48\textwidth]{pajengseq}
\caption{Original PajeNG's work-flow.}
\label{fig:pajengseq}
\end{figure}

This processing model has performance problems. When the trace files become larger the replaying time is extended in a prohibitive way. The graph~\ref{graph:pajeseq} shows a simple example: in the X axis are placed different trace file sizes and in the Y axis the run time in seconds of the of median thirty executions (The median was chosen because of some outliers which make it more significant than the average). In this we can observe that the time spent increases linearly according to the size of the files. So, given that massive parallel applications often produce huge traces, this becomes a serious fault for the tool.


\begin{figure}[!htb]
\includegraphics[width=0.48\textwidth]{seqVSsizes}
\caption{Sequential spent time to process different trace file sizes.}
\label{graph:pajeseq}
\end{figure}

\section{PajeNG Parallelization}\label{sec:pajepar}

The strategy used for the parallelization of the PajeNG intends to maximize the amount of data read from the disk. This approach was chosen due to our previous work which has noted that most of the time spent by the tool is led by the first two stages of the work-flow section~\ref{sec:pajeseqW}. The implementation presented here was built for shared memory architectures using C++11 threads. This consists in some modifications in the paj\'e trace file format and in the original work-flow which creates a new processing model that can also serve for a future distributed memory version.

The figure~\ref{fig:multiFileSTR} shows a representation of our parallelization strategy. Here we use some abbreviations that are: 
\begin{description}
\item[pFR] blocks are equal to \textit{PajeFileReader}.
\item[pED] blocks are equal to \textit{PajeEventDecoder}.
\item[pS] blocks are equal to \textit{PajeSimulator}.
\item[PC] blocks are equal to \textit{PajeContainer} event.
\end{description}
We stress that a new event type was created, the \textit{PajeTraceFile} event represented in the figure~\ref{fig:multiFileSTR} by PTF blocks. This new type contains two main information: its respective container and a file name. The single trace file used by the original PajeNG was split for each container found. In this way several trace files are generated, each with all information about a single container. Moreover a starter trace file was built containing only the creation of all containers and \textit{PajeTraceFile} events. That being said the strategy works as follows. In the top of the figure~\ref{fig:multiFileSTR} is presented the main thread. This processes the starter trace file and launches one thread for each \textit{PajeTraceFile} event (currently the maximum number of threads is controlled by the user). The threads basically run a new program with a part of the split trace file, each processing one container. Therewith we execute several flows similar to the sequential in parallel. The performance analysis for this strategy is presented in the section~\ref{sec:results}.


\begin{figure}[!htb]
\includegraphics[width=0.483\textwidth]{MultiFileSTR-BW}
\caption{Representation of our parallelization strategy for the PajeNG.}
\label{fig:multiFileSTR}
\end{figure}

\section{Performance Analysis}\label{sec:results}
We design several tests intending to prove the performance of the developed strategy. These are presented in the sub-sections below starting by an warm-up~\ref{sub:platform} that details some information about the used platform and metrics. The other sub-sections (~\ref{sub:pinfree}~\ref{sub:ssdhdd}~\ref{sub:strongweak}~\ref{sub:disk}) explain the approached metrics. 

\subsection{Experimental Platform and Warm-up}\label{sub:platform}
The tests were made over two different machines, one called /textit{turing} and other called /textit{bali}. These both are described below.
\begin{description}
\item[Turing] the machine is composed by four NUMA nodes, each with eight physical processors Intel(R) Xeon(R) CPU X7550 running at 2.00GHz. There is a total of 128 GB of RAM. The storage system is based on a RAID Dell PERC H700 SATA 3 6Gb/s of throughput and have several hard drives attached totalizing 5TB. 
\item[Bali] the machine is composed by two NUMA nodes, each with eight physical processors Intel(R) Xeon(R) CPU E5-2650 running at 2.00GHz. There is a total of 32 GB of RAM.
\end{description}
The data used in all graphs were collected from thirty executions for each different configuration. Moreover the execution times displayed in the graphs are the median value of the executions. The median was chosen because we note that there are outliers in some samples, which can disturb the results if we use the mean.

The analysis follows four metrics. The first, in the sub-section~\ref{sub:pinfree}, compares pinned threads (pin) with free threads (free). The following sub-section~\ref{sub:ssdhdd} verifies if there are differences between read the data from the HDD of the machines or from the SSD. The third metric in the sub-section~\ref{sub:strongweak} puts in contrast strong and weak scalability analysis. Finally the last sub-section~\ref{sub:disk} shows the concurrency by the disk according to the number of threads.


\subsection{Pin \emph{versus} Free}\label{sub:pinfree}

The graph~\ref{graph:turingPinVsFree} presents a comparison between pinned and free threads executing at Turing. The Y axis presents the median execution time for each configuration of number of threads in the X axis. A total of 63 trace files are used comprising 10GB of data that are stored on the HDD of the machine.

In this representation~\ref{graph:turingPinVsFree} we can see that most of the times the differences between the versions are very small except by the 8 threads configuration. The hypothesis for this variation is that since the threads share some data structures pinning them can cause less cache misses (we still performing some cache coherence tests so we cannot prove that yet). The tests reveal that from the 8 threads configuration until 32 we start to lose performance. It occurs because when we are using more than 8 threads the concurrency by the storage system increases significantly. This observation is highlighted with the disk tests in the sub-section~\ref{sub:disk}. 

\begin{figure}[!htb]
\includegraphics[width=0.45\textwidth]{turingPinVsFree}
\caption{Comparison between pinned threads versus free threads at Turing. There is the median of thirty executions for 2, 4 , 8, 16 and 32 threads processing trace files comprising 10GB}
\label{graph:turingPinVsFree}
\end{figure}

The graph~\ref{graph:baliPinVsFree} presents almost the same as the previous except by the different machine that is the Bali and the maximum number of threads (16 that is the number of physical cores of this machine). In this tests also note that the differences between the versions are very small. Other interesting thing is that in this machine we were not able to reproduce the same outliers for 8 threads and also the performance loss for 16 threads. This observations are under study.

\begin{figure}[!htb]
\includegraphics[width=0.45\textwidth]{baliPinVsFree}
\caption{Comparison between pinned threads versus free threads at Bali. There is the median of thirty executions for 2, 4 , 8 and 16 threads processing trace files comprising 10GB}
\label{graph:baliPinVsFree}
\end{figure}


\subsection{SSD \emph{versus} HDD}\label{sub:ssdhdd}

Th graph~\ref{graph:HDDvsSSDturing} shows the execution times for the HDD versus SSD of the Turing. The Y axis presents the median execution time for each configuration of number of threads in the X axis. Here we note that the execution time in the SSD is a little lower than the HDD even both having the same throughput. This happens because SSD works better than HDD for concurrent access~\cite{chen2011essential}.

\begin{figure}[!htb]
\includegraphics[width=0.45\textwidth]{HDDvsSSDturing}
\caption{Comparison between HDD and SSD at Turing. There is the median of thirty executions for 2, 4 , 8, 16 and 32 threads processing trace files comprising 10GB}
\label{graph:HDDvsSSDturing}
\end{figure}

\begin{figure}[!htb]
\includegraphics[width=0.45\textwidth]{HDDvsSSDbali}
\caption{Comparison between HDD and SSD at Bali. There is the median of thirty executions for 2, 4 , 8 and 16 threads processing trace files comprising 10GB}
\label{graph:HDDvsSSDbali}
\end{figure}

\subsection{Strong and Weak Scalability}\label{sub:strongweak}

\subsection{TimePerFile /emph{versus} TimePerRead}\label{sub:disk}






\section{Conclusion}\label{sec:conclusion}
The conclusion goes here.





\section*{Acknowledgment}

\
The authors would like to thank...


\bibliographystyle{IEEEtran}
\bibliography{mybib}
\end{document}

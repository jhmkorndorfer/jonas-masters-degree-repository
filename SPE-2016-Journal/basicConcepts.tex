\section{Basic Concepts}
\label{chap.BasicConcepts}
There are many ways to evaluate the performance of parallel
applications.
Common examples include the energy consumption, instructions per second, memory usage,
speedup, efficiency and so on. This section presents two of these
metrics: how to determine the speedup and the corresponding efficiency
achieved by a parallel program. The section also brings an overview about how
the NUMA (Non-Uniform Memory Access) architecture works. This
knowledge is essential to understand our performance analysis
Section~\ref{chap.analysis}, since the experiments were performed on
NUMA machines. For this, the section is divided in three subsections: 
Subsection~\ref{sec:AmdahlLaw} presents the Amdahl's
Law~\cite{amdahl1967} for strong scalability and the
Subsection~\ref{sec:GustafsonLaw} with the Gustafson's
Law~\cite{gustafson1988} for weak scalability. 
Subsection~\ref{sec:NUMAoverview} offers an overview about the NUMA
architecture.

\subsection{Amdahl's Law and Strong Scalability}\label{sec:AmdahlLaw}
The ideal speedup/efficiency of a parallel program would be linear.
Doubling the number of processing elements should halve the
runtime maintaining the efficiency at 100\%. The ideal speedup is hard
to obtain. Gene Amdahl have shown the
limit on a parallel program's speedup/efficiency as it executes with
more cores while solving the same problem size (strong scalability). The
Figure~\ref{fig:strongScalExample} illustrates such idea: the program
should ideally take $1/K$ amount of time to perform the result for the
same problem, with $K$ processes. Amdahl proposes some equations as follows. 
Equation~\ref{speedupAmdahlEq} shows the sequential
program's running time divided by the parallel program's running time
with $K$ processes, ultimately defining the speedup metric.
Amdahl's efficiency is shown in Equation~\ref{efficiencyAmdahlEq}: it
calculates how far the program is from the ideal speedup.

\begin{equation}\label{speedupAmdahlEq}
  Speedup(N,K) = \frac {Tseq(N,1)} {Tpar(N,K)}
\end{equation}

\begin{equation}\label{efficiencyAmdahlEq}
  Efficiency(N,K) = \frac {Speedup(N,K)} {K}
\end{equation}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.5\textwidth]{imgs/strongScalExample}}
  \caption{Visual example for strong scalability. Figure adopted
    from~\cite{kaminsky2013}.}
  \label{fig:strongScalExample}
\end{figure}

Gene Amdahl asserts that every parallel program has a sequential
fraction that limits the performance gains. So even increasing the
number of cores, the runtime will never be faster than such fraction's
time. Equation~\ref{AmdahlLawFormula} describes this interaction
providing a program's total running time using $K$ cores. The
sequential fraction $F$ is the total running time that must be
performed in a single core. When executing with one core, the
program's total execution time is $T$, the sequential portion
takes time $FT$, and the parallelizable portion takes
$(1 - F)T$. When using $K$ cores, the parallelizable
portion may experience an ideal speedup taking $(1 - F)T/K$ time,
but the sequential portion still takes $FT$ time. The sequential
portion $F$ therefore limits the possible parallelization gains. This is 
famous equation called Amdahl's Law~\cite{amdahl1967}.

\begin{equation}\label{AmdahlLawFormula}
  \begin{split}
    T (N ,K) & = F T(N ,1) + \frac{1 - F}{K} T(N,1)
  \end{split}
\end{equation}

\subsection{Gustafson's Law and Weak Scalability}\label{sec:GustafsonLaw}
John Gustafson has explained another way to check the scalability of a parallel
program when more cores are available. Differently from Amdahl's law
where the problem size is kept fixed, Gustafson propose an alternative
manner by letting the problem size change according to the number of
resources available. This is known as weak scaling as
exemplified by the Figure~\ref{fig:weakScalExample}: as the number of
cores increases, the problem size is also increased in the same
proportion. So ideally it should take the same amount of time to
perform a $K$ times larger problem. Therefore, the main idea is
how much we can increase the problem size/number of cores maintaining
the runtime. With that Gustafson asserts that as the problem size
increases, the program's parallelizable portion's running time also
increases. So how the program's sequential portion typically remains
the same, it will becoming less and less significative when
considering the program's total
running time. Thus the speedup still increases without hitting the
limit imposed by Amdahl's law.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.5\textwidth]{imgs/weakScalExample}}
  \caption{Visual example for weak scalability. Adopted
    from~\cite{kaminsky2013}.}
  \label{fig:weakScalExample}
\end{figure}

The main equations for weak scaling are the Sizeup and Efficiency. The
Sizeup equation~\ref{sizeupGustafsonEq} is the main metric for
measuring weak scaling. Considering the $T$ time of the sequential
program with the 1 problem divided by the $T$ time of the parallel
program with $K$ cores and $K$ problems. The Efficiency
equation~\ref{efficiencyGustafsonEq} tells how close a program is to
the ideal Sizeup. This is much like the strong scaling efficiency,
that is dividing the $Sizeup(N ,K)$ by $K$ number of
cores.

\begin{equation}\label{sizeupGustafsonEq}
  Sizeup(N,K) = \frac {N(K)} {N(1)} * \frac {Tseq(N(1),1)}{Tpar(N(K),K)}
\end{equation}

\begin{equation}\label{efficiencyGustafsonEq}
  Efficiency(N,K) = \frac {Sizeup(N,K)} {K}
\end{equation}

\subsection{The Non-Uniform Memory Access (NUMA) Achitecture}
\label{sec:NUMAoverview}

The Non-Uniform Memory Access (NUMA) architecture was designed to
improve the scalability limits of the Symmetric Multi-Processing (SMP)
architecture in which all memory access are posted to the same shared
memory bus. The SMP works relatively well for a small number of CPUs, however
with a larger number of CPUs there are much concurrency to access the
shared memory bus. Therewith NUMA intends to reduce these bottlenecks
by limiting the number of CPUs on a memory bus. The NUMA architecture
classifies memory into NUMA nodes in which a certain number
of cores reside. All cores of the same node have equivalent memory
access characteristics, and all nodes are inter-connected by means of a high
speed interconnect.

The Figure~\ref{fig:numaExample} shows a visual representation of what
is a NUMA architecture. Each rectangle presents a NUMA
node with its respective cores and local memory range. The black lines
mean the high speed interconnect mentioned above. The NUMA
architecture has some particular features that are very important for
a program running on it. All cores of the system can access the entire
memory of any node, however not at all with the same speed. 
Two concepts were created to differ the memory access pattern:
\textit{local node} that is when a core access the memory range of its
own node; and \textit{remote node} that occurs when a core allocates
memory that resides in another node local memory. The \textit{local node}
access is much faster than a \textit{remote node}. Therefore a
parallel program has to be prepared to execute in a NUMA machine,
otherwise it will lose performance. The development of a program for
NUMA commonly uses the \textit{NUMA aware} approach which means
maximize the local memory allocation.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.5\textwidth]{imgs/numaEx}}
  \caption{Simple visual representation of the NUMA architecture. Adopted
    from~\cite{barney2010introduction}.}
  \label{fig:numaExample}
\end{figure}

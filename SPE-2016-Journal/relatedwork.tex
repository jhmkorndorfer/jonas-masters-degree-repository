\section{Related Work}
\label{chap.relatedwork}
The goal of this section is to present an overview about some current high performance analysis tools and some famous explicit parallelization techniques/patterns. This has focus in the internal structure of the surveyed tools. The subsections are structured as follows. Subsection~\ref{manualPT} presents parallelization techniques. Subsection~\ref{perfATools} explains some performance analysis tools, also including the original and sequential PajeNG. The objective of latter is
to detail the main features of each tool and its processing model. 

%The last Subsection~\ref{sec:relateCon} makes a comparison between the surveyed tools and presents some conclusions.

\subsection{Explicit Parallelization Techniques and
  Patterns}~\label{manualPT}

The pipeline~\cite{mattson2004patterns} pattern describes a way to
handle data in parallel. It proposes the connection of tasks in a
producer–consumer relationship, basically creating a linear sequence
of stages. Conceptually all stages of the parallel pipeline are active
at once and can be updated as the data flows through them. There are
two basic approaches to implement the
pipeline~\cite{mccool2012structured}. The first sets a worker to
process some stage and the data bounded for that. The second puts a
worker to process a piece of data through all pipeline stages. The
differences between these approaches are their applicability: the
first approach is better for large stages and small data, while the
second is preferred for smaller stages and larger data. The
Figure~\ref{fig:pipeline} shows an abstraction of the pipeline pattern
implemented by the Intel TBB~\cite{intelTBB} library. This is a
modified version of the second approach commented above. In this case
a worker receives one of the five input data items and carries it
through as many stages as possible. When a worker finishes its task,
it checks if there are more data for processing. The pipelines are
commonly useful for serially dependent tasks which can be decoupled
forming a fixed number of stages. Therefore the pipeline speedup
scalability is limited by the slowest stage, for example: a pipeline
with three balanced stages achieves a maximum speedup of three, since
it is directly attached to the number of stages.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.25\textwidth]{imgs/pipeline}}
  \caption{A DAG model for the pipeline processing with five input
    items. Figure adopted from~\cite{mccool2012structured}.}
  \label{fig:pipeline}
\end{figure}

The stencil~\cite{mccool2012structured} pattern is an alternative
approach to map data which proposes that an elemental function can
access a set of data in an input collection, and not just a single
element. This pattern intends to provide parallel access to an input
collection where each worker pick up a set of fixed offsets for
processing. Stencil is commonly used for image filtering, median
filtering, motion estimation, simulations like fluid flow and lots of
others applications. The recurrence technique addresses a complex case
where loop iterations can depend one each other. This works like a map
but can use the outputs from a iteration of adjacent elements as
inputs for the next. Recurrence can be implemented with a sequence of
stencils. The outputs of stencils (that is offsets of data processed)
are iterated and can be reinterpreted as recurrences over
space-time. Recurrences can be used by several applications like:
image processing, sequence alignment, and also by performance analysis
tools to infer behavioral patterns of a program
execution~\cite{1998discovering, cornelissen2007}.

\subsection{Performance Analysis Tools}\label{perfATools}
This subsection presents some of the most important trace-based
performance analysis tools. The text have focus on the internal
structure of such tools intending to explain how they achieve high
performance on the processing of large amounts of data. This subsection includes the tools Vampir, Paraver, ViTE and finally the sequential PajeNG tool-set which is the main focus of this work.

\subsection*{Vampir}
\label{vampirTool}
The Vampir tool-set~\cite{nagel1996vampir} is a commercial
infrastructure for performance analysis of parallel programs built for
MPI, OpenMP, Pthreads, CUDA, and OpenCL. Currently at version
8.3\footnote{Vampir's website: https://www.vampir.eu}, the tool is
available for Unix, Windows, and Apple platforms. It is composed by
the Vampir GUI for interactive post-mortem visualization, the
VampirServer for parallel analysis, the VampirTrace instrumentation
and run-time tracing system, and the Open Trace
Format(\textit{OTF/OTF2}) as file format and access library.

The recommend code instrumentation and run-time measurement framework for the newest
versions of the Vampir tool-set is the Score-P\footnote{Score-P
available at: http://www.vi-hps.org/projects/score-p/}. This generates OTF2 file format. The Vampir performance analysis~\cite{brunst2010vampir7} tool provides
a framework which can quickly display the programs behavior at many
levels of detail. The latter uses the traces to generate several graphic
views. The tool allows profiling and event-based tracing. Profiling is
the summarized run-time behavior of programs with accumulated
performance measurements. Event-based analysis allows a more detailed
comprehension of the program's execution behavior. This approach
records timed events like function calls and message communication, in
a combination of timestamps, event type, and event specific
data. Tracing allows a more precise analysis and several different
representative ways of seeing the date like time-line views and
communication matrices.

The Figure~\ref{fig:master_timeline} shows one of the most useful
views supported by the Vampir. This is called master time-line view
which contains detailed information about functions, communication,
and synchronization events over the time. In the left side of the
Figure~\ref{fig:master_timeline} each row presents a single process
and, in the right side, the events that have occurred on it. The horizontal
axis shows the selected execution time interval. The different colors
represent a specific group of functions or synchronization events, for
example \texttt{MPI\_Send} belonging to the function group MPI.

The high performance achieved by Vampir comes with its component called VampirServer~\cite{knupfer2008vampir}. This is a parallel and
distributed client-server framework that provide
these features:
\begin{itemize}
\item The performance data are kept where it has been created,
  avoiding data transfer for processing;
\item The parallel/distributed process in each node increase the
  scalability of the analysis processing;
\item It works efficiently from every end-user platforms;
\item Large performance data can be remotely browsed and visualized
  interactively.
\end{itemize}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.8\textwidth]{imgs/master_timeline}}
  \caption{Master time-line view taken from Vampir's site.}
  \label{fig:master_timeline}
\end{figure}

The VampirServer architecture is based on a master-slave relationship
as shown in Figure~\ref{fig:VampirServerArch}. The MPI master process
does the communication with the clients and handle the workers. The
workers are a set of identical MPI processes, each one with a main
thread that does the communication with the master process. The
session threads are present in every MPI process of the architecture
and are dynamically created according to the number of clients. The
communication between local threads is made through shared buffers
that are synchronized by mutexes and conditional variables.

The worker's session threads are subdivided in three modules: trace
format, event data base and analysis. The trace format module is a
parser for some trace formats. The event data base module stores
objects divided in categories like functions, messages etc. Finally,
the analysis module performs the event data provided by the data base
module.

The master's session threads have different classification. These are
subdivided as: analysis merger, endian conversion and client comm,
like shown~\ref{fig:VampirServerArch}. The analysis merger combines
the results received from several workers. The endian conversion
converts the results to a platform independent format. Finally, the
client comm layer does the communication with the clients.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.8\textwidth]{imgs/VampirServerArch}}
  \caption{VampirServer architecture with several MPI workers
    processes in the left, and the master MPI process in the
    right. Figure adopted from~\cite{knupfer2008vampir}.}
  \label{fig:VampirServerArch}
\end{figure}

\subsection*{Paraver}\label{paraverTool}
Paraver~\cite{pillet1995paraver} is a post-mortem, trace-based tool
for performance analysis of parallel applications. This intends to
graphically display the behavior of a program's execution. Paraver
provides different filters to select what is going to be showed and
also offers some tools like timing and zooming, which improve the
understanding of such programs behavior.

Paraver implements its own trace format without semantics
associated. This means that the tool is very dynamic and easy to
extend for new performance data and/or programming models. The current
run-time measurement system of Paraver~\footnote{Paraver's website:
  http://www.bsc.es/computer-sciences/performance-tools/paraver/} is
the Extrae~\cite{alonso2012tools} which generates Paraver traces for
MPI, OpenMP, pthreads, OmpSs and CUDA.

The Paraver trace file format~\cite{ParaverTF} is open source and can
be summarized in three generic classifications:
\begin{itemize}
\item Events: defines a punctual event occurred in a given time-stamp
  that has a pair of integer values to represent its activity.
\item States: determines intervals of thread status. This type is
  heavily related with the parallel programming model.
\item Relation/Communication: establishes a relationship between two
  objects of the trace. This type is frequently used to describe
  records of the communication between: two processes (in MPI
  applications), task movement among threads (in OmpSs applications)
  or memory transfers (in CUDA/OpenCL applications).
\end{itemize}

The Paraver analysis framework~\cite{labarta2006performance} provides
three views generated from the traces. The first is called graphic
view: much similar to the Vampir's master time-line, it consists in a
representation of the program's execution behavior over the time. The second is a textual view that provides more
detailed information. The third is the analysis view: this allows the
user to select quantitative data to be displayed.

The Figure~\ref{fig:ParaverAnalysis} shows an example of the Paraver's
time-line graphic view. This was a survey about the performance of a
meteorological model using Paraver, NMMB/BSC-CTM
model~\cite{markomanolis2014performance}, and presents one execution
hour with 64 cores. The colors have specific semantics, for example:
the blue one is computation, the yellow one is MPI message, the red
one is MPI Wait and so on.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.8\textwidth]{imgs/ParaverAnalysis}}
  \caption{Paraver Graphic view (Time-line) example. One hour of
    simulation from NMMB/BSC-CTM model. Figure adopted
    from~\cite{markomanolis2014performance}.}
  \label{fig:ParaverAnalysis}
\end{figure}

Paraver explores an
intelligent selection of the traced information to gain performance. This is made by many
techniques to reduce the trace file size, like detecting periodic
structures, determine sets of events to be captured etc. The Paraver's
processing model is divided in three modules: filter, semantic and
representation, as depicted in the Figure~\ref{fig:ParaverArch}. The filter module intends to minimize the trace data size
through several selection and aggregation mechanisms. The semantic
module, generates a semantic value for each represented object which is associated with
information like event type/color, event name and so on. Finally, the
representation module translates the traces into visual
representations described above.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.8\textwidth]{imgs/ParaverArch}}
  \caption{Paraver internal structure. Figure adopted
    from~\cite{ParaverRM}.}
  \label{fig:ParaverArch}
\end{figure}

\subsection*{ViTE}\label{viteTool}
ViTE(Visual Trace Explorer~\footnote{ViTE's website:
  http://vite.gforge.inria.fr/index.php})~\cite{Vite2009} is an
open-source multi-format trace visualizer for performance
analysis. This tool do not have a specific tracing library, however
the developers recommend EZTRACE~\cite{trahay2011eztrace} or the
GTG(Generic Trace Generator)~\footnote{Available under the CeCILL-C
  license at: http://gtg.gforge.inria.fr/}. EZTRACE generates traces
in the Paje format~\cite{pajetracefile}. GTG generates and converts
traces in the Paje and in the previously seen OTF file formats. ViTE
intends to be the most compatible as possible with other analysis
tools: supports the formats Paje, OTF and TAU~\cite{shende2006tau}
(although not fully tested yet with the TAU). ViTE provides a
graphical interface that allows the user to browse the traces and
rendering modules to generate SVG or PNG files depicting traces to
export the results.

The performance of ViTE is correlated with its efficient data
structure and a fast rendering. The data structure is based on binary
trees that are built over sorted lists of known size. This
construction allows modifications that minimize the data stored, for
example: instances of states are recorded just by state changes, thus
avoiding store its start and duration. The binary tree structure is
also important for rendering the traces, which summarizes portions of
the data according to a resolution parameter. This resolution is a
portion of time that eliminates items that are too small to be
rendered. When applying a zoom-in operation, such resolution
decreases and the same process is used. The traces rendering module
was built with OpenGL. According to the
authors~\cite{coulomb2012open}, after benchmarking several rendering
solutions, this appear to be the most scalable one.

\subsection*{PajeNG}
\label{PajeNGTool}
PajeNG~\cite{pajeng} is a tool-set for trace replay built in C++. The tool
is capable to read files that follow the generic Paje trace format~\cite{pajetracefile}. The
tool-set provides four tools with different purposes:
\texttt{pj\_validate}, \texttt{pj\_dump}, \texttt{pajeng},
\texttt{libpaje}. The \texttt{pj\_validate} validates the integrity of
the trace data. The \texttt{pj\_dump} exports the processing results
in a CSV file format. The \texttt{pajeng} is a visualization tool that
generates views with the replayed traces. Finally the \texttt{libpaje}
is a library to process traces following the Paje format. Furthermore
\texttt{libpaje} is object oriented and can be easily modified or
extended. As it follows, we describe the Paje trace file format, how
performance analysis is conducted in the PajeNG framework, and how the
tool works to simulate (replay) traces in a sequential manner.

The Paj\'e trace file format is a self-defined, textual, and generic
format to describe the behavior of the execution of parallel and/or
distributed systems. The trace files are composed by two parts. The
first part (header) contains a definition for each event type with an unique
identification number, like shown in Listing~\ref{fig:traceDef}. The
second part, depicted in Listing~\ref{fig:trace}, contains one event
per line. The first field identifies the event type and the others are
separated by spaces or tabs. Those fields must follow the same order
presented in the header.

\lstset{ basicstyle=\fontsize{11}{9}\selectfont\ttfamily }
\begin{lstlisting}[caption=Example of event definition adopted
  from~\cite{pajetracefile}.,label=fig:traceDef,firstline=1,
  lastline=14]
  
                        % EventDef SendMessage 21
                        % Time date
                        % ProcessID int
                        % Receiver int
                        % Size int
                        % EndEventDef
                                            
                        % EventDef UnblockProcess 17
                        % Time date
                        % ProcessId int
                        % LineNumber int
                        % FileName string
                        % EndEventDef
\end{lstlisting}

\begin{lstlisting}[caption=Example of trace data adopted from~\cite{pajetracefile}.,label=fig:trace,firstline=1,
lastline=3]

                        21 3.233222 5 3 320 
                        17 5.123002 5 98 sync.c
\end{lstlisting}

The traced events in Paj\'e format are defined in five types of
conceptual objects: containers, states, events, variables and
links. The containers can be any monitored entity like a network link,
a process, or a thread. It is the most generic object and the unique
that can hold others, including other containers. States represent
anything that has a beginning and ending time-stamp. For example: a
given container starts blocked and then, after some time, changes to
free. This period of time characterises a state. Events are anything
remarkable enough to be uniquely identified and contain only one
time-stamp. Variables have information about the evolution of the
value of a variable along time. Finally, the links are the
relation/messages between two containers having a beginning and ending
timestamps.

There are many ways to generate traces in the Paje format: it can be
collected directly from the application execution, or converted from
other formats. Akypuera is an independent project that provides the
tracing library. This library provide a low memory footprint and low
intrusion tracing and generates binary traces that are converted to
Paje format. Furthermore Akypuera provide tools to covert traces from
several formats to Paje, like Rastro, TAU, OTF2 and OTF.

The PajeNG's framework has two main tools for performance
analysis. The \texttt{pajeng} is a visualization tool that can
generate views such as time-lines, and \texttt{pj\_dump} that exports
the results in a CSV-like format tailored to conduct R
analysis~\cite{ihaka1996r}.
%
The Figure~\ref{fig:pajengTimeLine} presents an example of a time-line
view generated by using the \texttt{pajeng} tool. Each line represents
a container. The rectangles are different types of States described by
the traces (see trace format description above) and represented by
distinct colors. The user can select drawn objects to retrieve more
detailed information about that. This case the user have selected a
\texttt{State} object and received its detailed information in the
selected point and in the bottom of the program as shown in
Figure~\ref{fig:pajengTimeLine}. The tool also provides space-time
zoom operations by using the scroll bars in the bottom and in the
right side.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=0.8\textwidth]{imgs/pajengTimeLine}}
  \caption{Example of time-line view from \texttt{pajeng}.}
  \label{fig:pajengTimeLine}
\end{figure}

The Listing~\ref{fig:dumpng} shows an example of the CSV output text
content generated by the \texttt{pj\_dump} tool. Each line presents
one simulated object with its respective features and values. For
example, the fifth row of the Listing~\ref{fig:dumpng} describes a
State for the container ``node32'', of the type ``SERVICE'', starting time
``538 seconds'', finish time ``548 seconds'', duration ``10 seconds'',
imbrication level ``0'' and with the value ``free''. This content can be
easily used to create several statistics and plots by using
R\footnote{R Project's website: https://www.r-project.org/} software
environment for statistical computing and graphics.

\lstset{ basicstyle=\fontsize{9}{8}\selectfont\ttfamily }
\begin{lstlisting}[caption=Example of \texttt{pj\_dump} output
  data.,label=fig:dumpng,firstline=1, lastline=8]
  
  Container, 0, 0, 0, 1205, 1205, 0 
  Container, 0, L1, 0, 1205, 1205, node32 
  State, node32, PM, 1149.0000, 1205.0000, 56.0000, 0.0000, normal 
  State, node32, SERVICE, 537.0000, 538.0000, 1.0000, 0.0000, reconfigure 
  State, node32, SERVICE, 538.0000, 548.0000, 10.0000, 0.0000,free
  State, node32, SERVICE, 548.0000, 553.0000, 5.0000, 0.0000, booked 
  Variable, node32, power, 0.0000, 1205.0000, 1205.0000, 1000000000.00
\end{lstlisting}

\subsubsection*{PajeNG's Work-Flow}\label{sec:pajeSeq}
The \texttt{libpaje} does the needed processing for the traces
replay. This works with a single trace file each time and does the
processing in a sequential way. The \texttt{libpaje} follows a
work-flow composed by three main components, as depicted in the
Figure~\ref{fig:pajengseq} proceeding from the left to the right. The
component \texttt{PajeFileReader} starts the processing by the trace's
reading. This catches chunks of fixed size from the trace file and
send them to the second component. The \texttt{PajeEventDecoder}
receives such chunks, splits them into lines, and transforms each line
in a generic event-object according to its definition (see Paje's
trace format description above). The last component,
\texttt{PajeSimulator}, implements different functions for each event
type to stack up each event-object into the correct stack of its
respective container.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/paje_sequencial_ing}}
  \caption{Original libpaje's sequential work-flow.}
  \label{fig:pajengseq}
\end{figure}

The Paje structure is strongly connected, that
is, the \texttt{PajeDecoder} must wait for data provided by the
\texttt{PajeFileReader}, and the \texttt{PajeSimulator} waits for
objects coming from the \texttt{PajeEventDecoder}. This way, while a
chunk of data is being processed, the \texttt{PajeFileReader} is
stopped. Thus this model has performance problems since the processing 
time is linearly proportional to the size of the input trace file. The
Figure~\ref{fig:pajengseqRunTime} shows an example of this problem,
where the time proportionally increases to the input size. The
\texttt{X} axis presents the input trace file size, and the \texttt{Y}
axis, the minimum run-time collected from 30 executions (these
executions were performed in the machine \texttt{turing} which is
described in Section~\ref{chap.analysis}). Each line represent a
different disk type, HDD or SSD, according to the caption. The results
were very similar for both disks, however this was probably due to the
\texttt{turing}'s SSD that is not much faster than the HDD. Therefore
these results demonstrate that a sequential approach is unable to
handle high performance parallel applications which produce thousands
of Gbytes of trace data when executed. Our work builds on this by
attempting to solve this issue with parallelization and a change in
the Paje file format. Our solution is described in next section.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=.8\textwidth]{imgs/graph013-seqHDDvsSSD-jonas-diss}}
  \caption{PajeNG's run-time ranging the input size and disk type, HDD
    vs SSD. This results were collected from the minimum value of 30
    executions performed in the machine \texttt{turing} (see platform
    description in Section~\ref{chap.analysis}).}
  \label{fig:pajengseqRunTime}
\end{figure}



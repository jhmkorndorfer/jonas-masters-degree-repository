\section{Introdução}

A atual crescente demanda por poder de computação tende para o
desenvolvimento de aplicações paralelas. Sistemas modernos de alto
desempenho compreendem de milhares a milhões de unidades de
processamento conectadas por redes hierárquicas complexas. O
desenvolvimento de tais aplicações é tarefa complicada, afetada por
pelo menos dois fatores: o primeiro é a alta escalabilidade dos
computadores atuais; o segundo é a falta de uma execução
determinística, uma vez que os nós de processamento são
independentes. Algumas bibliotecas como OpenMP~\cite{dagum1998openmp}
e MPI~\cite{gropp1999mpi2} são utilizadas para facilitar o
desenvolvimento destas aplicações. No entanto, mesmo com bibliotecas
de suporte, o sucesso no desenvolvimento de uma aplicação paralela de
alto desempenho depende de um mapeamento correto dos recursos
disponíveis.

A identificação de recursos mal utilizados e possíveis gargalos de
processamento passa por uma boa análise de desempenho com ferramentas
que consigam observar muitas entidades ao longo de grandes períodos de
tempo. Esta observação é frequentemente feita pelo rastreamento do
programa, o qual coleta métricas significativas de desempenho. Os
dados coletados geralmente iniciam-se a partir do nível de aplicação:
a maioria das ferramentas se concentra em registrar estados locais e
globais do programa, a quantidade de dados transferidos por mensagens
e contadores para funções específicas. Tais dados permitem a detecção
de vários padrões complexos como comunicações tardias, sincronização 
custosa ou efeitos de comboio~\cite{schnorr2012detection}. No entanto,
a técnica de rastreamento pode gerar grandes arquivos de rastro,
chegando a ordem de gigabytes de dados brutos. Portanto o
processamento destes dados deve ser eficiente para que permitam uma
análise de desempenho rápida e útil.

Ao longo dos anos, várias ferramentas baseadas em rastros foram
desenvolvidas para ajudar na análise de desempenho de aplicações
paralelas. Seu grande desafio é processar e representar a enorme
quantidade de dados comportamentais coletados de aplicações
paralelas. Os dados são geralmente utilizados para simular e
reconstruir o comportamento da execução de um programa. Isto
frequentemente resulta em representações visuais como de espaço/tempo, 
grafos de comunicação entre outras~\cite{schnorr2009}. A maioria das ferramentas existentes, como
Vampir~\cite{muller2007developing}, Scalasca~\cite{scalasca_2010} e
TAU~\cite{shende2006tau}, concentram-se no processamento de formatos
rastro com semântica pré-determinada. Tais formatos geralmente tem
como alvo aplicações desenvolvidas com bibliotecas como OpenMP e
MPI. No entanto, nem todas aplicações utilizam tais bibliotecas e
portanto, algumas vezes, estas ferramentas perdem utilidade. Existem
outras ferramentas que possuem uma abordagem mais dinâmica. Algumas
destas são: Paraver~\cite{pillet1995paraver}, Pajé~\cite{paje2000} e
PajeNG~\cite{pajeng}. Nestes casos, cada uma tem seu próprio formato
de arquivo de rastro, sem uma semântica específica. Desta forma
conseguem atingir uma gama maior de aplicações paralelas. Por outro
lado, tais ferramentas ditas genéricas geralmente apresentam baixo
desempenho quando trabalham com grandes rastros, ou utilizam técnicas
para reduzir o tamanho dos dados possivelmente perdendo informações
importantes.

Este trabalho concentra-se na otimização do processamento dos rastros
realizado pelo conjunto de ferramentas PajeNG. Este funciona com o
formato de rastro Pajé~\cite{pajetracefile}, o qual não está associado
semanticamente a nenhuma biblioteca. PajeNG implementa diversas
ferramentas que ajudam na análise de desempenho, como a composição de
visualizações ilustrando o comportamento de uma aplicação ao longo do
tempo e/ou a exportação dos resultados em formato textual para análise
com R~\cite{ihaka1996r}. O funcionamento do PajeNG é sequencial, com
isso seu desempenho diminui quando utilizado para processar grandes
arquivos de rastro. Neste trabalho apresentamos uma estratégia de
paralelização para o PajeNG, pretendendo melhorar seu desempenho e
aumentar sua escalabilidade. Nós apresentamos desde o desenvolvimento
da estratégia, até seu funcionamento e análise de desempenho. As
análises realizadas abordam diversos aspectos e apresentam resultados
bem promissores, escalando o PajeNG cerca de oito vezes em
computadores com um único disco rígido.

Este artigo está organizado da seguinte forma. A
Seção~\ref{sec:firstpage} apresenta os trabalhos relacionados. A
Seção~\ref{sec:pajeng_original} detalha a versão sequencial do
PajéNG. Nossa abordagem paralela da ferramenta é descrita na
Seção~\ref{sec:pajeng_parallel} juntamente com a análise de
desempenho, na Seção~\ref{sec:results}. As conclusões são apresentadas
no final, na Seção~\ref{sec:conclusion}.

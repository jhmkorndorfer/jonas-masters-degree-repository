\section{Análise de desempenho}
\label{sec:results}

Esta seção apresenta uma análise do desempenho obtido pela estratégia
de paralelização da ferramenta PajeNG. Após apresentar os detalhes
experimentais tanto da plataforma quanto da metodologia de análise,
começamos a apresentação com uma visão geral do desempenho obtido com
gráficos de aceleração (\emph{speedup}) e eficiência, prosseguindo com
uma investigação da razão pela queda de desempenho a partir de
um determinado nível de paralelismo. Esta investigação nos leva a
correlacionar o comportamento da aplicação com os detalhes
arquiteturais, sugerindo modificações na estratégia de paralelização.

%free /versus/ pin
%ssd /versus/ hdd
%bali /versus/ turing

\subsection{Plataforma e metodologia experimental}\label{sub:platform}

As configuração das duas plataformas experimentais que foram
utilizadas é resumida na Tabela~\ref{table.plataformas}. A primeira é
uma máquina Dell PowerEdge R910 identificada como {\bf turing}, com 32
núcleos físicos, organizados em quatro processadores Intel(R) Xeon(R)
CPU X7550 de 2.00GHz cada um com oito núcleos. Os processadores têm
acesso não uniforme à memória (NUMA), sendo que cada processador está
em um nó NUMA.  Com relação a numeração dos núcleos físicos para os
nós NUMA, no caso da turing, os núcleos são identificados com um
deslocamento de 4 em 4. Na tabela, apenas os núcleos físicos do nó
NUMA 0 são identificados. A memória principal desse sistema é de
128GBytes. O disco sólido é um Corsair Force 3, com 64 GBytes,
enquanto que o disco rígido é um Hitachi HUC151414CSS60 e um Western
Digital WD10TPVT-00U configurados em RAID.
%
A segunda plataforma é uma máquina SGI C1104G-RP5 identificada como
{\bf bali}, com 16 núcleos físicos, organizados em dois processadores
Intel(R) Xeon(R) E5-2650 de 1.2GHz, cada um com oito núcleos. A
máquina tem dois nós NUMA. A memória principal desse sistema é de
64GBytes. O disco sólido é um Samsung SSD 840 de 500GBytes, enquanto
que o disco rígido é um Seagate ST91000640NS de 1 TBytes.



\begin{table}[!htb]\centering
\begin{tabular}{lll}\toprule
                        & {\bf turing}        & {\bf bali} \\\toprule
Fabricante              & Dell PowerEdge R910 & SGI C1104G-RP5 \\
Modelo Processador      & Intel X7550 2GHz    & Intel E5-2650 1.2Ghz \\
Processadores           & 4                   & 2 \\
Núcleos/Proc.           & 8                   & 8 \\
Total de Núcleos        & 32                  & 16 \\
Nós NUMA                & 4 (0: 0,4,8,12,16,20,24,28)                   & 2 (0--7, 8--15) \\
Qtdade de Memória       & 128 GBytes          & 64 GBytes \\
Disco rígido            & RAID                 & {\small Seagate ST91000640NS 1TBytes} \\
Disco sólido            & {\small Corsair Forse 3 64GBytes} & {\small Samsung SSD 840 de 0.5TBytes}
\\\bottomrule
\end{tabular}
\caption{Configuração das plataformas experimentais.}
\label{table.plataformas}
\end{table}

Cada combinação experimental foi executada pelo menos 30 vezes de
forma a diminuir a possibilidade que erros de medição afetem a
interpretação. Uma análise preliminar de cada bateria de testes
identificou que a maioria das observações apresentam uma distribuição
gaussiana, indicando normalidade. As observações que não se enquadram
perfeitamente em uma distribuição normal são aquelas onde a sobrecarga
da execução e gargalo de desempenho se manifestaram, como a análise de
desempenho desta seção pretende apresentar. Foi feita uma metodologia
experimental com os seguintes fatores:

\begin{description}
\item[Localidade] A localidade dos fluxos de execução, também
  conhecida como afinidade, tem dois níveis de medição: com os fluxos
  de execução livres (\emph{free}), onde o escalonador do sistema
  operacional pode migrar esses fluxos para otimizar o tempo de
  execução e acesso à memória, ou com os fluxos pinados
  (\emph{pinned}), onde definimos um determinado mapeamento no início
  da execução e este não muda. Nesse segundo caso, foi feito um
  mapeamento linear de acordo com a numeração dos núcleos dados pelo
  sistema operacional. É importante salientar que a
  numeração fornecida na plataforma turing difere daquela da bali.

\item[Quantidade] A quantidade de fluxos de execução foi avaliada com
  cinco níveis que compreendem: uma execução paralela com apenas um
  fluxo de execução (1), que consideramos a execução ``sequencial''
  servindo inclusive como referência para o cálculo de aceleração, e
  em seguida com 2, 4, 8, 16 e 32 fluxos de execução.

\item[Tipo de disco] Esse fator teve dois tipos de medição: com um
  disco sólido e com um disco rígido. A integralidade dos dados de
  entrada foi registrada em cada um desses discos. O objetivo é
  comparar o desempenho de leitura dependendo da tecnologia de
  fabricação e a correlação com as arquiteturas de cada plataforma.

% \item[Escalabilidade] Dois tipos de teste de escalabilidade foram
%   realizados. O primeiro deles é o teste de escalabilidade forte, onde
%   o tamanho do problema a ser processado se mantém fixo enquanto se
%   aumenta a quantidade de fluxos de execução. O segundo é o teste de
%   escalabilidade fraca, onde o tamanho do problema aumenta na mesma
%   proporção que o aumento no grau de paralelismo. Cada tipo pode
%   evidenciar características diferentes da aplicação na plataforma
%   experimental escolhida.
\end{description}

As medições foram realizadas ao longo de sete baterias de testes
guiadas de acordo com a análise descrita abaixo. Cada bateria de
testes foi configurada de acordo com a interpretação dos resultados
da bateria anterior.

\subsection{Visão geral preliminar}

%1 Overview do speed-up and efficiency
%1.1 Strong Weak scalability (HDD e SDD)

A visão geral do desempenho obtido está demonstrada na
Figura~\ref{fig.speedup-efficiency}, com (a) aceleração e (b)
eficiência. Cada um dos quatro gráficos de aceleração tem no eixo
horizontal a quantidade de fluxos de execução, enquanto que o eixo
vertical mostra a aceleração obtida. Para auxiliar a análise, a
aceleração ideal é representada como uma linha sem pontos. Esses
gráficos estão organizados em uma matriz onde horizontalmente tem-se
as duas plataformas -- bali e turing -- e verticalmente as duas
estratégias de localidade -- livre (\emph{Free}) e fixas
(\emph{Pin}). Os gráficos de eficiência, representados do lado direito
da figura, têm organização idêntica. Nestes, a linha contínua
representa a eficiência ideal.


\begin{figure}[!htb]
\centering
\begin{tabular}{cc}
\centering
%these plots were generated with bali1-turing2-6seq-speedup.r 
\includegraphics[width=.45\linewidth]{images/perfanalysis/speedup.pdf}&
\includegraphics[width=.45\linewidth]{images/perfanalysis/efficiency.pdf}
\\
(a) & (b) 
\end{tabular}
\caption{Aceleração e eficiência medidas nas plataformas bali e
  turing.}
\label{fig.speedup-efficiency}
\end{figure}

A interpretação dos dados representados na
Figura~\ref{fig.speedup-efficiency} indica que a aceleração na
plataforma bali se distancia de forma linear da curva da aceleração
ideal até a quantidade de 16 fluxos de execução quando os dados de
entrada estão em disco sólido (SSD). Isto se confirma através dos
gráficos de eficiência para bali, onde há uma eficiência mínima de
aproximadamente 50\% com 16 fluxos de execução. Já na plataforma
turing, observa-se que o desempenho acompanha a aceleração ideal
somente até quatro fluxos de execução. Com 8, 16 e 32 há uma queda
brusca da eficiência com dados tanto em disco rígido quanto sólido na
plataforma turing.

Em suma, uma possível explicação para a queda de desempenho a partir
de oito fluxos de execução pode ser o fato de que todos os dados de
entrada estão registrados em um único disco. Um aumento do paralelismo
de leitura inevitavelmente fará com que a concorrência de acesso ao
disco penalize a aplicação a partir de uma certa quantidade de fluxos
de execução. Neste caso, devemos continuar a investigação de forma a
identificar quando a concorrência de acesso em modo leitura no disco
começa a prejudicar o desempenho da aplicação. Para isto, verificamos
a variabilidade dos tempos de execução quando aumenta-se o
paralelismo a seguir.

\subsection{Verificação da variabilidade experimental}

%2 Por que o desempenho cai tanto com 8 threads? Teste da variabilidade nas medições
%2.1  Observação da alta variabilidade das medições com 8 threads no gráfico boxplot
%3 Apresentação das medições dos reads no gráfico com 2, 4, 8, 16, e 32 threads HDD e SDD

A Figura~\ref{fig.variability}.(a) mostra a variabilidade do tempo de
execução dos diferentes experimentos com disco rígido (HDD, cor preta)
e sólido (SSD, em cinza) utilizando gráficos de caixa (\emph{boxplot})
de Tukey~\cite{boxplots} variando-se a quantidade de fluxos de
execução, sempre com afinidade fixa. Estes gráficos representam a
mediana (traço no meio da caixa), o primeiro e o quarto quartil e
valores distantes (\emph{outliers}). Eles são úteis pois mostram a
distribuição das medições. Os gráficos são incrementados com pontos
que representam cada execução feita, de forma a auxiliar no
entendimento da variabilidade.

A interpretação dos dados representados na
Figura~\ref{fig.variability}.(a) indica uma estabilidade nas medições
na máquina bali, inclusive quando esta encontra-se com 16 fluxos (um
por núcleo físico). Já na máquina turing, observa-se a grande
variabilidade do tempo de execução com oito fluxos de
execução. Através da observação da distribuição de pontos, observa-se
adicionalmente que a mediana não representa de forma adequada a
dualidade dessas medidas para execuções tanto em HDD quando em SSD com
oito fluxos de execução, medidas essas que ora estão próximas ao valor
superior da caixa, ora próximas ao valor inferior.

\begin{figure}[!htb]
\centering
\begin{tabular}{ccc}%x}{\textwidth}{XXX}
\setlength\tabcolsep{0pt} % default value: 6pt
%this plot is generated with turing-variability.r
\includegraphics[width=.3\linewidth]{images/perfanalysis/turing-variability.pdf}&
%these plots were generated with turing3-read-duration.r
\includegraphics[width=.3\linewidth]{images/perfanalysis/turing3-read-duration-2-4-8.png} &
% there is also a pdf file for this last one
\includegraphics[width=.3\linewidth]{images/perfanalysis/turing3-read-duration-8t-hdd.pdf}
\\
(a) & (b) & (c)
\end{tabular}
\caption{(a) Variabilidade do tempo de execução dos experimentos bali e
  turing; (b) tempos de leitura para HDD/SSD com 2, 4 e 8 fluxos; e
  (c) tempos de leitura para 8 fluxos em HDD.}
\label{fig.variability}
\end{figure}

A variabilidade de tempo de execução para oito fluxos de
execução na máquina turing nos leva a observar outras métricas de
desempenho. Para tentar entender a origem de tal variabilidade,
registramos o tempo necessário para a realização da função
\texttt{read}. Esta função é responsável pela leitura dos arquivos de
entrada do simulador, que lê 1.5MBytes de dados cada vez que
é chamada. A expectativa com base nestas medições é a observação de um
aumento do tempo de \texttt{read} a medida que aumentamos o
paralelismo.

A Figura~\ref{fig.variability}.(b) mostra a duração dos tempos de
\texttt{read} na turing variando-se o tipo do disco (HDD e
SSD) e a quantidade de fluxos de execução de (2, 4 e 8), todos com
afinidade fixa. O eixo vertical de cada gráfico representa a duração
dessa função de leitura. No eixo horizontal representa o tempo (em
segundos) do momento em que a função foi chamada. Cada ponto é uma chamada a
função \texttt{read}. Optou-se por representar os casos de 2, 4 e 8
fluxos uma vez que com 16 e 32 o tempo de execução piora.

Várias evidências comportamentais podem ser retiradas da
Figura~\ref{fig.variability}.(b). Para o caso com 2 fluxos de
execução, observa-se uma maior estabilidade da duração das leituras no
disco sólido (SSD). Com 4 e 8 fluxos, e independente do tipo de disco,
começa a se tornar marcante a dualidade dos tempos de
leitura. Retirando-se as faixas de tempo com menor intensidade, se
torna marcante a quantidade de medições em duas faixas de tempo. A
Figura~\ref{fig.variability}.(c) representa apenas os tempos de
leitura para o caso com 8 threads e no disco rígido (HDD): fica claro
a dualidade dos tempos de leitura, ora melhores (valores menores no
eixo vertical), ora piores (caso contrário). A questão é porquê os
tempos de execução não são sempre estáveis em torno de um único valor
médio ou mediano. Para melhor investigar as razões dessa dualidade, e
correlacionar com outras características da plataforma, precisamos de uma nova métrica de desempenho relacionada
ao tempo de leitura de um dos arquivos de entrada do Pajé em sua
versão paralela.

%Indícios de correlação com NUMA Nodes. Necessita melhor métrica.

\subsection{Tempo de leitura de cada arquivo e correlação com nós NUMA}

Como explicado anteriormente, cada fluxo de execução na versão do PajeNG
paralelo é responsável pela leitura, decodificação e simulação dos
eventos. Sendo assim, podemos definir uma métrica que indique o tempo
que leva para realizar a leitura completa de um arquivo por um
determinado fluxo de execução. Essa métrica pode ser utilizada então
como um comparador confiável do desempenho entre os diferentes fluxos
de execução, uma vez que as demais etapas ocorrem sem interação com o
disco, e que estamos fixando a afinidade de cada fluxo de execução em
um determinado núcleo físico.

%4 Nova métrica calculada sobre o tempo de leitura do arquivo
%5 Discussão sobre pinando threads por NUMA Node

A Figura~\ref{fig.maxtimetofinish}.(a) apresenta os dados calculados
baseados nesta nova métrica, organizados da seguinte forma. Três
execuções foram feitas, identificadas pelas colunas da matriz de
gráficos (execuções número 4, 5 e 6 das 30 medições que foram
realizadas). Estas execuções foram conduzidas nos dois tipos de disco,
identificados pelas linhas dessa mesma matriz de gráficos. Cada
gráfico tem no eixo horizontal a quantidade de fluxos de execução
utilizada, e no eixo vertical a nova métrica calculada: a quantidade
de tempo necessária para completar a leitura do arquivo para a memória
principal. A cor de cada caixa está relacionada ao nó NUMA onde o
fluxo de execução foi executado: preto indica o nó NUMA 0, enquanto
que o cinza claro indica o nó NUMA 1. Os pontos considerados fora da
escala (\emph{outliers}) são desenhados como pontos negros.

\begin{figure}[!htb]
\centering
\begin{tabular}{ccc}
\centering
%this plot is generated with turing5-maxtimetofinish.r
\includegraphics[width=.45\linewidth]{images/perfanalysis/turing5-maxtimetofinish.pdf} &
%this plot is generated with turing6-maxtimetofinish.r (four numa nodes)
\includegraphics[width=.45\linewidth]{images/perfanalysis/turing6-30exec_average-gray.pdf} 
\\
(a) & (b)
\end{tabular}
\caption{(a) Três execuções (4, 5 e 6) onde a quantidade de tempo
  necessário para ler um arquivo de entrada é representada, de acordo
  com o tipo do disco e o nó NUMA onde o fluxo de execução foi
  executado; e (b) gráfico de caixa do tempo de leitura do arquivo
  baseado em 30 execuções considerando todos os quatro nós NUMA, para
  os dois tipos de disco.}
\label{fig.maxtimetofinish}
\end{figure}

A interpretação da Figura~\ref{fig.maxtimetofinish}.(a) elucida a
dualidade dos tempos de leituras apresentados na subseção
anterior. Isso ocorre pois percebe-se uma diferença na quantidade de
tempo necessária para realizar a leitura quando os fluxos de execução
com afinidade fixa se executam no nó NUMA 0 em relação ao nó NUMA
1. Essa diferença evidencia que os fluxos de executados fixos no nó
NUMA 0 são capazes de ler inteiramente os arquivos de entrada cerca de
30\% mais rapidamente. Isso é explicável pela organização arquitetural
dos nós NUMA da máquina turing, onde o barramento PCI -- que conecta a
interface de disco ao processador -- se encontra conectado apenas ao
nó NUMA 0. Quando o fluxo de execução fixado em outro nó NUMA
diferente do 0 quer ler um arquivo, os dados devem obrigatoriamente
ser roteados através do nó NUMA 0, atrasando-os.

%6 Apresentação do gráfico de tempo de leitura por NUMA Node
%  -> usar turing-5, turing-6 e turing-7

A Figura~\ref{fig.maxtimetofinish}.(b) segue a mesma organização que o
gráfico em (a), com exceção que as medidas das 30 execuções foram
agregadas em uma bateria experimental que utiliza todos os nós NUMA da
plataforma turing. Confirma-se portanto a diferença significativamente
menor no tempo de leitura para fluxos de execução que se executam no
nó NUMA 0. Outro aspecto que fica marcante é o aumento do tempo de
leitura para fluxos executando no melhor nó NUMA 0 conforme se aumenta
a quantidade de fluxos de execução. Isso indica que o aumento do
paralelismo com fluxos em outros nós NUMA prejudica os fluxos que se
executam no nó NUMA 0.


%\subsection{Sumário}




% \subsection{Performance Analysis on Bali}\label{bali}

% \subsubsection{Strong and Weak Scalability}\label{sub:bstrongweak}

% \subsubsection{Pin \emph{versus} Free}\label{sub:bpinfree}

% \subsubsection{SSD \emph{versus} HDD}\label{sub:bssdhdd}

% \subsection{Performance Analysis on Turing}\label{turing}

% \subsubsection{Strong and Weak Scalability}\label{sub:tstrongweak}

% \subsubsection{Pin \emph{versus} Free}\label{sub:tpinfree}

% \subsubsection{SSD \emph{versus} HDD}\label{sub:tssdhdd}

% \subsubsection{NUMA nodes \emph{versus} Disk Throughput}\label{sub:numavdisk}


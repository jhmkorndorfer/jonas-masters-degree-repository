
%
%  $Description: Author guidelines and sample document in LaTeX2e$
%
%  $Author: ienne, paulley $
%  $Date: 2002/04/15 11:20:59 $
%  $Revision: 1.0 $
%

\documentclass{ieee}

\usepackage[utf8]{inputenc} %Put this package so you can type Pajé
                            % instead of Paj\'e
\usepackage{url}
\usepackage{graphicx}
\usepackage{color}
\usepackage[english]{babel}
%------------------------------------------------------------------------- 
% 
% Use \documentclass[pagenumbers]{ieee}
%
% to produce page numbers, bottom centered, in the output. Default is 
% no page numbers for camera-ready copy.
%
%------------------------------------------------------------------------- 

\usepackage{times}

% Red review marker
\newcommand{\review}[1]{\textcolor[rgb]{1,0,0}{#1}}

\begin{document}

\title{PajeNG Multi-trace File Parallelization Strategy}

\author{Jonas H. M. Korndorfer\\
Email: jhmkorndorfer@inf.ufrgs.br\\
\and
Lucas Mello Schnorr\\
Email: schnorr@inf.ufrgs.br\\
\and
Universidade Federal do Rio Grande do Sul\\
Instituto de Inform\'atica \\
}

\maketitle
\thispagestyle{empty}

\begin{abstract} Trace-based observation of a parallel program
  execution is one of the most helpful techniques for performance
  analysis. This approach frequently have to deal with large traces,
  which creates a great challenge: how to process and analyze such
  information in an efficient way. We address this challenge using
  PajeNG, a performance analysis toolset. In its current form, PajeNG
  is sequential and fails to scale for large trace files. Here we
  present the development of a parallelization strategy for PajeNG,
  together with a preliminary performance analysis. The experiments
  executed on a NUMA machine show that we scale up to four threads
  linearly with good efficiency.
\end{abstract}



%-------------------------------------------------------------------------
\section{Introduction}

The current increasing demands of computing power are tending for the
development of parallel applications~\cite{sadashiv2011cluster} that
better use the multi-core architectures existing today. Such
development is complex task, affected by at least two factors: the
first is the high scalability of current supercomputers; the second is
the lack of deterministic execution since processing nodes are
independent. Two executions with the same entry can demonstrate
completely different behavior. Some libraries like
OpenMP~\cite{dagum1998openmp} and MPI~\cite{gropp1999mpi2} are used to
help the development of these parallel applications. However even with
supporting libraries, the success of the development of a high
performance parallel application depends on a correct mapping of the
available resources.

The behavior understanding of the application execution is one of the
most powerful methods to identify potential bottlenecks and misused
resources. Frequently this is done by tracing an application
collecting performance counters and important events during the
execution. This method generally results in large trace files reaching
the order of gigabytes of raw performance data.  The processing of
this data must be efficient to conduct a fruitful performance
analysis.

Over the years, several tools were developed to help the performance
analysis of parallel applications. Most of these, like
Vampir~\cite{nagel1996vampir} and Scalasca~\cite{scalasca_2010}, are
focused in the processing of traces with specific semantic which
targets libraries like OpenMP and MPI. A minor part of these, like
PajeNG~\cite{pajeng}, have its own trace file format without
associated semantics. This work focuses on the optimization of the
PajeNG on its task of processing trace files. The great advantage of
this tool is that with its trace file format without
semantics~\cite{pajetracefile}, it can be used to represent data
coming from several different applications without any
modification. As of today, PajeNG trace processing is sequential, so
large amounts of data spend much time to be processed. The work
described in this paper presents a parallelization strategy for the
PajeNG intending to improve its performance when dealing with very
large trace files.

This text is structured as follows. Section~\ref{sec:pajeseq} details
the sequential PajeNG. Section~\ref{sec:pajepar} explains how our
parallelization strategy works and the modifications applied to
PajeNG. The Section~\ref{sec:results} presents a preliminary
performance analysis of our strategy. Section~\ref{sec:related} mentions some studies/tools similar to our. Finally we conclude in
Section~\ref{sec:conclusion} with a discussion and suggestions for
future work.

%-------------------------------------------------------------------------
\section{Original PajeNG Implementation}\label{sec:pajeseq}
  The PajeNG~\cite{pajeng} is a trace-based tool-set
for performance analysis designed in C++. It works with its own trace file
format and provides four tools/libraries with different proposals:
\textit{pj\_validate}, \textit{pj\_dump}, \textit{pajeng},
\textit{libpaje}. The \textit{pj\_validate} is used to analyse the
integrity of the trace data, outputing some statistics. The
\textit{pj\_dump} can export the results of the processing in a
CSV-like file format. The \textit{pajeng} is a visualization tool that
generates interactive space/time representations of the
traces. Finally the \textit{libpaje}, which is the main focus of this
work, is an open source library that does the trace processing in all
other tools contained in PajeNG.

\subsection{Description of the Paj\'e Trace Format}\label{traceformat}
This subsection describes the Pajé trace file format. We
have to include these concepts here because they are very
important for the comprehension of how the PajeNG and our
strategy works. The Pajé trace file format is self-defined, textual, and generic. The
genericity allows anyone to describe the behavior of the execution of
any kind of parallel and/or distributed systems whose behavior can be
described by timestamped events. The traces in the Paje format can be
one of five types:
\begin{description}
\item [Container] can be any monitored entity like a network link, a
  process, or a thread. It is the most generic object and the only one
  that can hold others, including other containers;
\item [State] represents anything that has a beginning and ending
  time-stamp. For example: a given container starts blocked and then,
  after some time, changes its state to free. This period of time is a
  state;
\item [Variable] represents the evolution of the value of a variable
  along time. A common example is the number of exchange messages
  along time;
\item [Link] can be used to represent the causality relation between
  two containers having a beginning and ending timestamps. The
  traditional example is a message passing between two processes;
\item [Event] is anything remarkable enough to be uniquely identified
  and has only one time-stamp.
\end{description}

\subsection{Workflow Features \& Problems}\label{sec:pajeseqW}

The \textit{libpaje} is responsible for the trace replay, working with
one trace file each time through a sequential processing. This is done
by three main classes: \textit{PajeFileReader},
\textit{PajeEventDecoder} and \textit{PajeSimulator}. The
Figure~\ref{fig:pajengseq} illustrates \textit{libpaje}'s workflow,
from left to right. The \textit{PajeFileReader} reads chunks of the
trace file, sending them to the second class. In the next step, an
object of the \textit{PajeEventDecoder} class receives the chunks and
splits them into lines which are transformed in generic event
objects. The flow of events generated by the decoder are received by
the \textit{PajeSimulator}. For each event, this component simulates
the event behavior changing the current data of each container. The
history of changes generated by all events is kept in memory by the
simulator.

\begin{figure}[!b]
\includegraphics[width=0.468\textwidth]{pajengseq}
\caption{Original PajeNG's workflow.}
\label{fig:pajengseq}
\end{figure}

This processing model has performance problems. When the trace files become larger the replaying time is extended in a prohibitive way. The graph~\ref{graph:pajeseq} shows a simple example: in the X axis are placed different trace file sizes and in the Y axis the run time in seconds of the of median of thirty executions (The median was chosen because of some outliers which make it more significant than the mean). In this we can observe that the time spent increases linearly according to the size of the files. So, given that massive parallel applications often produce huge traces, this becomes a serious fault for the tool.

\begin{figure}[!htb]
\includegraphics[width=0.46\textwidth]{seqVSsizes}
\caption{Sequential spent time to process different trace file sizes.}
\label{graph:pajeseq}
\end{figure}

%-------------------------------------------------------------------------
\section{PajeNG Parallelization}\label{sec:pajepar}
The strategy used for the parallelization of the PajeNG intends to maximize the amount of data read from the disk. This approach was chosen due to our previous work~\cite{korndorferfirst} which has noted that most of the time spent by the tool is led by the first two stages of the workflow: \textit{PajeFileReader} and \textit{PajeEventDecoder} section~\ref{sec:pajeseqW}. The solution described here comprises two new changes: a change in the Paje file format to allow multi-file traces subsection~\ref{sub:traceM}; and the adoption of C++11 threads in the libpaje workflow subsection~\ref{sub:libMulti}.

\subsection{Paj\'e Trace file Modifications}\label{sub:traceM}
The support for multiple trace files was made by modifications in the Paj\'e trace file format. Such modifications consist in create a new event type called \textit{PajeTraceFile} and split the trace file in several pieces. The informations contained in this event are: its type, container and a file name or path. These informations will be used by the \textit{PajeSimulator} to start new threads. Therefore the \textit{PajeTraceFile} event works like a mark which indicates where are the remaining informations of a trace file and when start a new execution flow/thread.

The splitting of the trace file can be done in several ways. For the first experiments of our strategy we adopt the follows metric. The trace file was split for each \textit{container} event found. Therefore, each piece contains all informations of one container. Besides was created a starter trace file. This file only stores \textit{container} and \textit{PajeTraceFile} events. So when the \textit{PajeSimulator} performs such trace, it throws several flows in separated threads that will process each one a different \textit{container}.

\subsection{Parallel libpaje Workflow}\label{sub:libMulti}

The libpaje had to be modified in two ways: first to support the new event type and second to use C++11 threads for the parallel processing. The processing of \textit{PajeTraceFile} events is made by a new class named \textit{PajeParallelProcessor}. This class have implemented a queue of tasks and a pool of workers. These workers consist in C++11 threads and the pool size is user defined. The queue of tasks is filled by \textit{PajeTraceFile} events.

The modifications described above end up building a new work-flow for the libpaje. The figure~\ref{fig:multiFileSTR} is shown how such workflow runs. The main thread is presented in the top of the figure~\ref{fig:multiFileSTR}. The initial steps of its processing are equal to the sequential: starting by the \textit{PajeFileReader} (pFR) sending to the \textit{PajeEventDecoder} (pED) and finally to the \textit{PajeSimulator} (pS), like described in the subsection~\ref{sec:pajeseqW}. The differences begin when a \textit{PajeTraceFile} event (PTF) arrives in the \textit{PajeSimulator}. In this case the \textit{PajeSimulator} inserts the \textit{PajeTraceFile} event into the tasks queue of the \textit{PajeParallelProcessor} described above (the \textit{PajeParallelProcessor} is not presented in the figure~\ref{fig:multiFileSTR}, it is abstracted by the dotted lines). In our strategy, the main thread processes the starter trace file (remember that it is for our trace file splitting approach subsection~\ref{sub:traceM}). Therefore it works like a producer just filling the queue of tasks. 

\begin{figure}[!htb]
\includegraphics[width=0.44\textwidth]{MultiFileSTR-BW}
\caption{Representation of our parallelization strategy for the
PajeNG.}
\label{fig:multiFileSTR}
\end{figure}

The subsequent steps, right below of the figure~\ref{fig:multiFileSTR}, have presented the workers which will consume the tasks queue. Each worker withdraws a \textit{PajeTraceFile} event from the queue and according to its information, \textit{container} (PC) and trace file name/path, it launches a new workflow in a separated thread. These new workflows are also equal to the sequential.

\section{Experiments and Results}\label{sec:results}

This section presents the results obtained by our experiments with the
parallelization strategy for PajeNG described above. We have defined
two different configurations for the threads locality: free threads
and pinned threads. For both, we have executed thirty executions for
each different number of threads: 1, 2, 4, 8 and 16 threads
respectively for a total of 270 executions.

The experimental platform is a NUMA-machine with  four
NUMA nodes, each with eight physical processors Intel(R) Xeon(R) CPU
X7550 running at 2.00GHz. There is a total of 128 GB of RAM. The
storage system is based on a RAID Dell PERC H700 SATA 3 with 6Gb/s of
throughput and with several hard drives attached totalizing 5TB. 
%\subsection{Speedup}\label{sub:speedup}

The plot of Figure~\ref{fig:speedup} shows the speedup obtained by our strategy
comparing both versions: pinned version in blue and free version in
red. The results in this plot are based on the mean of the execution times. As we can see the speedup is
nearly linear until four threads for both versions. For eight threads
we note that the speedup decreases significantly in both versions,
however the pinned version presents better results.  After eight threads the
strategy become to lose performance. It probably occurs because of the
great concurrency of the threads reading the disc, However we
  cannot show evidence of that since we are still investigating why we
  loose so much performance when we reach 8 threads.

\begin{figure}[!htb]
\includegraphics[width=0.404\textwidth]{speedup}
\caption{Speedup graph of the multi-trace file strategy for the PajeNG.}
\label{fig:speedup}
\end{figure}

\section{Related Work}\label{sec:related}
Our work is closely related to prior research in the area of
parallel application tracing and profiling~\cite{pillet1995paraver, gelabert2011extrae, nagel1996vampir, wolf2008usage}. Trace-based tools like Vampir and Extrae collect application traces that are very attached to the semantics of the used programming libraries. They have good scalability but are not dynamic like PajeNG. The most similar tool of PajeNG is the Paraver~\cite{pillet1995paraver} that also have an own trace file format. However this tool achieves high performance by reducing the information in the trace file which can cause information loss.

\section{Conclusion}\label{sec:conclusion} The PajeNG is a very
usefull toolset for the performance analysis of several parallel
applications. The sequential version loses much of its utility when it
has to process large traces. Therefore we have developed
a parallelization strategy to better use the capacity of this toolset. The first results
are very optimistic with linear scaling PajeNG up to four threads. The
work is under study and further experiments are being developed to
improve even more the scalability. Some of these are: the correct
mapping of the threads location and how optimize the disk
usage. Besides, this implementation creates opportunities for future
work, since each thread are independent it can be easily used to the
implementation of a distributed version.
\bibliographystyle{ieee}
\bibliography{ieee}

\end{document}

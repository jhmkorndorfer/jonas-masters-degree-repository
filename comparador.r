#!/usr/bin/Rscript

# Execute this script like this (an example):
# ./comparador.r result1.txt result2.txt
# Supposing each file has one measure per line

library(plyr)
library(ggplot2)

args <- commandArgs(trailingOnly = TRUE)
datasets = c()
for (i in args) {
  novo = c(i)
  datasets = c(datasets, novo)
}
print(datasets)

read = function(what){
  read.csv(paste('', what, '', sep=''),sep=';')
}

d=data.frame()
for(name in datasets){
  r = read.csv(name, header=FALSE)
  print(median(r$V1))
  r = data.frame(t1=r$V1,name=name)
  d = rbind(d,r)
}

p <- ggplot(d, aes(x=name, y=t1,color=name))
plot( p + geom_boxplot() + geom_jitter(alpha=.4) )

#!/usr/bin/Rscript

# Execute this script like this (an example):
# ./density.r `find | grep \.data$ | grep bali1/ssd | grep strong/pin`

options(echo=TRUE)
pdf("density_plots.pdf")
par(mfcol=c(3,2))
args <- commandArgs(trailingOnly = TRUE)
for (i in args) {
    nome <- unlist(strsplit(i, "/"))[3]
    df <- read.csv (i)
    names(df)[1] <- "V"
    ordered <- df[order(df[1]),]

    dens <- density(df$V, kernel="gaussian", bw="nrd0")
#
# Compute some measures of location.
#
    n <- length(dens$y)
    dx <- mean(diff(dens$x))
    y.unit <- sum(dens$y) * dx
    dx <- dx / y.unit
    x.mean <- sum(dens$y * dens$x) * dx
    y.mean <- dens$y[length(dens$x[dens$x < x.mean])]
    x.mode <- dens$x[i.mode <- which.max(dens$y)]
    y.mode <- dens$y[i.mode]
    y.cs <- cumsum(dens$y)
    x.med <- dens$x[i.med <- length(y.cs[2*y.cs <= y.cs[n]])]
    y.med <- dens$y[i.med]
#
# Plot the density and the statistics.
#
    plot(dens, main = nome,  xlab="Tempo de Execução (segundos)", ylab="Densidade")


    print(paste(nome, median(df$V), sep=" = "))
#
# Plot the histogram
#
    hist(df$V, breaks="FD", main = i, col="grey", border="white", probability=TRUE,add=TRUE)

#
# Plot the mean, median and mode
#
    temp <- mapply(function(x,y,c) lines(c(x,x), c(0,y), lwd=2, col=c), 
               c(x.mean, x.med, x.mode), 
               c(y.mean, y.med, y.mode), 
               c("Blue", "Yellow", "Red"))

    lines(dens)
}


# See this StackOverflow for details on mode/median/mean
# http://stackoverflow.com/questions/12455998/how-to-obtain-multiple-lines-in-a-single-density-plot-with-a-corrected-scale

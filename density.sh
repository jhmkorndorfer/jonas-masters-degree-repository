#!/bin/bash

#README: simple run this script with ./density.sh

P="./density.r"

KEYS="turing/hdd/strong/pin \
      turing/hdd/strong/free \
      turing/hdd/weak \
      turing/ssd/strong/free \
      turing/ssd/strong/pin \
      bali1/hdd/strong/pin \
      bali1/hdd/weak \
      bali1/ssd/strong/pin \
      bali1/ssd/strong/free \
      bali1/ssd/weak"

for KEY in $KEYS; do
    $P `find | grep \.data$ | grep $KEY`
    mv density_plots.pdf `echo $KEY | sed "s/\//-/g"`.pdf
done


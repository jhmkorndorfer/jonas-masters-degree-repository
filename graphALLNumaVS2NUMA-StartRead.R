#+begin_src R :results output :session :exports both
df <- read.csv ("strong_scalability_data_Dissertation/allT5-T6WithExpNames.csv", sep=" ");
names(df) = c("ThreadID", "File", "Start", "End", "Duration", "Core", "NUMANode", "NP", "Execution", "Disk", "Version", "Machine");
df <- df[!df$File == "root.trace", ];
df <- df[!df$File == "containerroot.trace", ];
df$NP <- as.factor(df$NP);
df$ThreadID <- as.factor(df$ThreadID);
df$Execution <- as.factor(df$Execution);
#df$Machine <- as.factor(df$Machine);
df$File <- as.factor(df$File);
df$Core <- NULL;
#+end_src

#+begin_src R :results output graphics :file (org-babel-temp-file "figure" ".png") :exports both :width 1200 :height 400 :session
library(ggplot2);
library(dplyr);
df2 <- df[df$NP == 16 & df$Execution == 6 & df$Disk=="SSD", ];
print(summary(df2));
head(df2)

k2 <- df2 %>% group_by(ThreadID, Machine) %>% mutate(MinStart=min(Start), NewStart=Start-MinStart, NewEnd=NewStart+Duration, NewSize=Duration) %>% as.data.frame();


ggplot(k2, aes(x=NewStart, xend=NewEnd, y=File, yend=File))  +
  theme_bw(base_size = 9) + 
  theme(legend.position="none") +
  geom_bin2d() + 
  xlab("Start -> Duration") +
  geom_segment(size=1,color="Red",lineend = "square") +
   xlim(-0.5,4) +
  facet_grid(~Machine ~.);

ggplot(k2, aes(x=NewStart, xend=NewEnd, y=File, yend=File))  +
  theme_bw(base_size = 9) + 
  theme(legend.position="none") +
  geom_bin2d() + 
  xlab("Start -> Duration") +
  geom_segment(size=1,color="Red",lineend = "square") +
  xlim(-0.1,8) +
  facet_grid(~Machine ~.);

ggplot(k2, aes(x=NewStart, xend=NewEnd, y=File, yend=File))  +
  theme_bw(base_size = 9) + 
  theme(legend.position="none") +
  geom_bin2d() + 
  xlab("Start -> Duration") +
  geom_segment(size=1,color="Red",lineend = "square") +
  # xlim(-1,50) +
  facet_grid(~Machine ~.);

#!/usr/bin/Rscript

## strong/pin contra strong/free

## turing, bali1
## hdd, ssd


## ./turing/hdd/strong/pin
## ./turing/hdd/strong/free

## ./turing/ssd/strong/pin (não tem)
## ./turing/ssd/strong/free (não tem)

## ./bali1/hdd/strong/pin
## ./bali1/hdd/strong/free (não tem)

## ./bali1/ssd/strong/pin
## ./bali1/ssd/strong/free

## let's focus on turing/hdd/strong/pin
## ./bali1/hdd/strong/pin/2tFixCore.data
## ./bali1/hdd/strong/pin/4tFixCore.data
## ./bali1/hdd/strong/pin/16tFixCore.data
## ./bali1/hdd/strong/pin/8tFixCore.data

library(plyr)
library(ggplot2)

options(echo = TRUE)
pdf("pin-free.pdf")

loadCSV <- function (file, threads, type) {
    print(file)
    print(threads)
    print(type)
    temp <- read.csv (file, header = FALSE, stringsAsFactors = TRUE)
    names(temp)[1] = "Time"
    temp$Threads = as.factor(threads)
    temp$Type = type
    temp
}

s = "sequential"
p = "pinned"
f = "free"
conf <- data.frame(
    file = c("./bali1/ssd/strong/pin/2tSSDFixCore.data",
        "./bali1/ssd/strong/pin/4tSSDFixCore.data",
        "./bali1/ssd/strong/pin/8tSSDFixCore.data",
        "./bali1/ssd/strong/pin/16tSSDFixCore.data",
        "./bali1/ssd/strong/free/2tSSD.data",
        "./bali1/ssd/strong/free/4tSSD.data",
        "./bali1/ssd/strong/free/8tSSD.data",
        "./bali1/ssd/strong/free/16tSSD.data",
        "./bali1/ssd/strong/free/seqSSD.data"),
    threads = c(2, 4, 8, 16, 2, 4, 8, 16, 1),
    type = c(p,p,p,p,f,f,f,f, s))
merged <- rbind.fill(apply(conf, 1, function(x) loadCSV(x[1], x[2], x[3])))

merged$Threads <- factor(merged$Threads)#, levels = merged$Threads[order(merged$Threads)])

p = ggplot(merged, aes(factor(Threads), y = Time, color = Type))
p + geom_boxplot()

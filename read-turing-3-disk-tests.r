
read_disk_test_turing_3 <- function (files)
{
    df = data.frame()
    for (file in files){
        r = read.csv(file, sep=" ", header=FALSE)#, stringsAsFactors=FALSE)
        names(r) = c("ThreadID", "File", "Start", "End", "Duration", "Core", "NUMANode", "Threads", "Execution", "Disk", "Fix")
        r <- r[!r$File == "root.trace", ]
        r <- r[!r$File == "containerroot.trace", ]

        # #Fix dates
        m <- min(r$Start)
        r$Start <- r$Start - m
        r$End <- r$Start+r$Duration

        # #Fix ThreadIDs
        m <- min(r$ThreadID)
        r$ThreadID <- r$ThreadID - m

        # #to factors
        r$ThreadID <- as.factor(r$ThreadID)
        r$Core <- as.factor(r$Core)
        r$NUMANode <- as.factor(r$NUMANode)
        r$Threads <- as.factor(r$Threads)
        r$Execution = as.factor(r$Execution)

        df = rbind(df,r)
    }
    df
}

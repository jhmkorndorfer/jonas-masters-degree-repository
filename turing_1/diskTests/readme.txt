Nas pastas encontram-se os dados coletados referentes a leitura de dados do disco.
Os testes foram executados para o HDD e SSD da turing e estão respectivamente armazenados nas pastas hdd e ssd.
 
Estes dados contém os valores de leitura do disco de 5 execuções para cada configuração de número de threads diferente. 2-4-8-16-32.

Para cada configuração de N de threads são gerados 2 arquivos diferentes que contém os dados das 5 execuções. 

O conteúdo dos arquivos esta explicado abaixo:


(N de threads)timePerFile
Col1 - Nome do arquivo em leitura || Col2 - Tempo total de litura do arquivo || Col3 - Read M/S (calculo de Megabytes por segundo)

(N de threads)timePerRead
Col1 - Nome do arquivo em leitura || Col2 - Tempo de leitura de um Chunk     || Col3 Read M/S (calculo de Megabytes por segundo)



A linha "End Exec: (time)" determina o fim de uma execução em ambos arquivos.
 

#!/bin/sh

FILES=$*

echo $FILES

for file in $FILES; do
    THREADS=`echo $file | cut -d"." -f2 | cut -d"t" -f1`
    EXEC=`echo $file | cut -d"." -f1 | sed "s/0//g"`
    DISK=`echo $file | cut -d"t" -f2 | cut -d"P" -f1 | cut -d"T" -f2`
    PIN_TEST=`echo $file | grep Pin | wc -l`
    if [ $PIN_TEST -eq 1 ]; then
	PIN="Pin"
    else
	PIN="Free"
    fi
    echo "$file \t $THREADS \t $EXEC \t $DISK \t $PIN"

    sed -i "s/$/ $THREADS $EXEC $DISK $PIN/" $file 
done
